%% ====================================
% This is the main function that calls 
% Creating Groundtruth, Selects sensor planning, and evlauates.
% ====================================

clear all
close all
clc

%% Initialization ====================
totalNumSamples = 500; 
totalNumRun = 10;
bParallel = true;

% Select Sampling config
%['random'; 'APF'; 'vertical_sweep'; 'horizontal_sweep'];
%[samplingMethod, selectionType] = getSamplingConfig();
samplingMethod = 'APF';
selectionType = 'iteratively';

% Currently only 'basic' Kernel DM+V is available.
% Select from : ['basic', 'wind', 'time', 'learn'];
gdmMethod = 'basic';   

% Select form: 'coverage', 'plume', 'distance', 'KL', 'source'
measure = 'KL';

% flag to build a map with all data as ground truth.
bCreateGroundTruth =  true;

% flag to calculate GDM for the given cellsize and kernel width.
bCreateGDM = true;

% flag to run evaluation
bEvaluatePerformance = false;

% flag to display the grpahs generated in the evaluation step
bShowVisualize = false;
bNormalizeData = true;
bNormalizeTime = false;
bVisualizeData = false;

% set input experiment
% experimentLabel = 'corridor', 'smallnet',  
%                   'no_obstacle_16x4', 'with_obstacle_up_16x4'] 
%                   'mox_sensor_model',  'with_obstacle_two_60x20'
% for 'with_obstacle_two_60x20' you need to add the path manually if you want to change datasset to 
% the ones with change in conditions such as gas source location, and release rate.
experimentType = 'simulation_ros';
experimentLabel = 'mox_sensor_model';

% select for which type of GDM we want to create visualization: kernel DM+V
% (2D) and/or TD Kernel DM+V (2DTD)
gdmType = '2D';


% set the start time - this is used to create a folder in the result path
startTime = clock;
strStartTimeId = [sprintf('%i',startTime(1)) '_' ...
    sprintf('%02i',startTime(2)) '_' sprintf('%02i',startTime(3)) '-' ...
    sprintf('%02i',startTime(4)) '_' sprintf('%02i',startTime(5)) '_' sprintf('%02.0f',startTime(6))];


inputFolder = 'data';
resultFolder = 'result';
resultFilePath = [resultFolder '/' experimentType '/' experimentLabel '/' gdmType '/' strStartTimeId];

if ~exist(resultFilePath, 'dir')
    mkdir(resultFilePath);
end

%% Create Groundtruth ================
if bCreateGroundTruth
    [P, PVar] = doCreateGroundTruth(experimentType, experimentLabel, gdmType, ...    
        inputFolder, resultFolder, bVisualizeData, bNormalizeData, bNormalizeTime);
end

%% Select Experiment =========================================
% select experiment corresponding to the input experimentLabel
    [experimentType, fileName, startInd, endInd, sweepsInd, mapCenter, mapSize, ...
        sourceLocation, gasType, experimentSubDescr] = selectExperiment(experimentLabel);
margin = [0, 0];

%% Select Sensor Planning ====================================
samplingPoints = zeros(totalNumSamples, 2);

% Select Sampling Positions
if (strcmp(samplingMethod, 'APF'))
    combination = selectSensorPlanning();
%     combination = [0.0 0.0 0.0 1.0; ...
%         0.0 0.1 0.0 0.9; ...
%         0.1 0.1 0.0 0.8; ...
%         0.1 0.8 0.0 0.1; ...
%         0.0 0.5 0.0 0.5];
else
    combination = [0.0 0.0 0.0 0.0]; %selectSensorPlanning();
    samplingPoints = getSamplingLocations(totalNumSamples, samplingMethod, mapSize, margin);
end

%% Sensor Planning ============================================
coeff = combination(combination(:,3) == 0, :);
startIdx = 1;
endIdx = size(coeff, 1);

if (bCreateGDM)
    if (bParallel)
        %PARPOOL close force
        delete(gcp('nocreate'))
        parpool('local',2)
    end

    if (bParallel)
        parfor count=startIdx:endIdx
            for runNum=1:1:totalNumRun
                samplingPoints = getSamplingLocations(totalNumSamples, samplingMethod, mapSize, margin);
                doCreateGDM(P, PVar, ...
                    samplingMethod, selectionType, coeff(count,:), ...
                    samplingPoints,totalNumSamples, runNum, ...
                    experimentType, experimentLabel, gdmType, ...  
                    inputFolder, resultFolder, ...
                    bVisualizeData, bNormalizeData, bNormalizeTime); 
            end
        end
    else
        for count=startIdx:endIdx
            for runNum=1:1:totalNumRun
                samplingPoints = getSamplingLocations(totalNumSamples, samplingMethod, mapSize, margin);
                doCreateGDM(P, PVar, ...
                    samplingMethod, selectionType, coeff(count,:), ...
                    samplingPoints,totalNumSamples, runNum, ...
                    experimentType, experimentLabel, gdmType, ...  
                    inputFolder, resultFolder, ...
                    bVisualizeData, bNormalizeData, bNormalizeTime);
            end
        end
    end
    
     if (bParallel)
        delete(gcp('nocreate'))
     end

end

%% Evaluation =====================================================
if (bEvaluatePerformance)
    for i=startIdx:endIdx
        doEvaluatePerformance(experimentLabel, samplingMethod, selectionType, coeff(i,:), ...
            totalNumRun, totalNumSamples, bShowVisualize, measure);
    end
end


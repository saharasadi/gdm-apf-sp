function [ locationList ] = getSamplingLocations( totalNumSamples, samplingType, ...
    mapSize, margin)
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here
    
    locationList = zeros(totalNumSamples, 2);

            
    if (strcmp(samplingType, 'random'))
        RandStream.setGlobalStream(RandStream('mt19937ar','seed',sum(100*clock)));
        locationList = rand(totalNumSamples,2);
        locationList(:,1) = locationList(:,1) * (mapSize(1,1) - margin(1,2));
        locationList(:,2) = locationList(:,2) * (mapSize(1,2) - margin(1,1));
    else
        y = [0.2:0.4:3.8]';
        x = [0.25: 0.53: 15.8]';

        cnt = 1;
        if (strcmp(samplingType, 'sweep-horizontal'))
            for j=1:1:size(y,1)
                if (mod(j,2) == 0)
                    for i=1:1:size(x,1)
                        [x(i,1) y(j,1)]
                        locationList(cnt, :) = [x(i,1) y(j,1)];
                        cnt = cnt + 1;
                    end
                else
                    for i=size(x,1):-1:1
                        [x(i,1) y(j,1)]
                        locationList(cnt, :) = [x(i,1) y(j,1)];
                        cnt = cnt + 1;
                    end
                end
                
            end
                       
        else 
            if (strcmp(samplingType, 'sweep-vertical'))
                for i=1:1:size(x,1)
                    if (mod(i,2) == 0)
                        for j=1:1:size(y,1)
                            locationList(cnt, :) = [x(i,1) y(j,1)];
                            cnt = cnt + 1;
                        end
                    else
                        for j=size(y,1):-1:1
                            [x(i,1) y(j,1)]
                            locationList(cnt, :) = [x(i,1) y(j,1)];
                            cnt = cnt + 1;
                        end
                    end
                end

            end
        end
    end   
    
    plot(locationList(:,1), locationList(:,2), '--*');

end


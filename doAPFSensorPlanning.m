function [] = doAPFSensorPlanning(runNum, coeff, samplingType, selectionType)

    %% CONFIG SENSOR PLANNING EVALUATION ==================================
    % here all combinations of importance weights of objective functions
    % are set, number of iterations for running each experiment and number 
    % of suggested points at each iteration is set (and  repulsion ).
    % These values of course, can be sent as an inpput to the sensor
    % planning, probably this will be done in next versions ;-)
    
    maxItr = 300;
    n_sp = 10;
    maxRepuls = 1;
    klValue = [];
    
    %% selectSensors();====================================================
    [sensorsUsed, bNormalizeSensorData, sensorsUsedDescr, ...
        expClassDescr] = selectSensors();

    %% selectExperiment();=================================================
    [experimentType, experimentLabel, experimentSubDescr, logFileType, ...
        mapCenter, mapSize, ...
        srcLocation, ...
        initInd, endInd, sweepsInd, ...
        calibrationFile, ...
        time, margin] = selectExperimentSimulation();

    %% SELECT GDM META PARAMETERS =========================================
    [cellSize kernelWidth] = selectGDM();
    
    %% set output path ====================================================
    %%coeff = [0.1 0.1 0.8]
    
    msgDate = datestr(now, 30);
    msgRunNum = num2str(runNum);
    gdmConfig = sprintf('c%.2f-s%.2f', cellSize, kernelWidth);
    spConfig = sprintf('M%.1f-V%.1f-R%.1f-C%.1f', coeff(1,1), coeff(1,2), coeff(1,3), coeff(1,4)); %!!!!! COEFF
    
    %outputPath = ['output/' 'SensorPlanning-' msgDate '-' spConfig];
    outputPath = ['output/' 'SensorPlanning-' msgRunNum '-' spConfig];
    
    mkdir(outputPath);
    
    experimentDescription = ['KL-' experimentType '-filament100_fulldata'];
    preOutputFile = [outputPath '/' experimentDescription '-' gdmConfig];    
    
    %% cofigure saving and visualization ==================================
    % in the next version I will remove these since, data will be saved in
    % mat files here and later, with others functions viualization will be
    % done.
    
    bVisualizeData = false;
    bVisualizePredModel = false; 
    bVisualizaKL = false;
    
    bSaveResult = true;
    bSaveDescription = true;
    bSaveSrc = false; %true;
        
    %% select input data ==================================================
	experimentPath = 'data';
	inputFile = [experimentPath '/' experimentLabel '.log'];

    %% extractInputDataFromSimSP();========================================
    inputData = extractInputDataFromSimSP(inputFile,sensorsUsed,...
        bNormalizeSensorData,calibrationFile);
    
    %timeTarget = time(2);
    simData = inputData((inputData(:,3)  < (mapSize(1,1) - margin(1,2))), :);
    
    simData(:,2) = ...
        (simData(:,2) - min(simData(:,2)))./(max(simData(:,2))-min(simData(:,2)));

    if (bVisualizeData)
        plot3(simData(:,3), simData(:,4), simData(:,2), '*');
    end

    %set of feasible answers (where there is a measurement available)
    %A = [1:1:size(simData,1)];
    % A = find(inputData(:, 8));
    croppedNumOfDataPoints = size(simData,1);
    numOfDataPoints = croppedNumOfDataPoints;    
    
    %% Model #P ===========================================================
    [ P PVar kGdmvMap_P ] = doBuildGDM(simData, time, mapCenter, mapSize);
     
    P = P ./sum(P);
    PVar = PVar ./sum(PVar);
    P = P ./repmat(sum(P,2),[1 size(P,2)]);
    PVar = PVar ./repmat(sum(PVar,2),[1 size(PVar,2)]);
    
    if (strcmp(samplingType, 'random'))
        modelFile = [preOutputFile '-' 'FullModel' '.mat'];
        save(modelFile, 'kGdmvMap_P');
    end
    
    %% SELECT A RANDOM INITIAL POSITION ===================================
    X = [];
    bVerbose = false;
    pos = [];
   
    %for now, the initial sensor id is selected randomly
    RandStream.setGlobalStream ...
         (RandStream('mt19937ar','seed',sum(100*clock)));
    
    rd = randi(size(simData,1)); %received as input
    selectedIdx = rd;
    pos =  [pos; simData(rd,3) simData(rd,4)];
    selectedSensor = find((simData(:,7) == simData(rd,7)) & (simData(:,8) == simData(rd,8)));

    X = [X; selectedSensor];
    subSimData = simData(X(:,1), :);
    
    [ Q QVar predModel] = doBuildGDM(subSimData, time, mapCenter, mapSize);
 
    %% SAMPLING ITERATIONS ================================================
    gdmConfig = sprintf('c%.2f-s%.2f', cellSize, kernelWidth);
    experimentDescription = ['KL-' experimentType '-filament100_APF'];
    preOutputFile = [outputPath '/' experimentDescription '-' gdmConfig];  
    
    model = [];
    rndList = [];
    
    if (strcmp(samplingType, 'random'))
        RandStream.setGlobalStream ...
         (RandStream('mt19937ar','seed',sum(100*clock)));
    
        rndList = rand(maxItr,2);
        rndList(:,1) = rndList(:,1) * (mapSize(1,1)-margin(1,1));
        rndList(:,2) = rndList(:,2) * (mapSize(1,2)-margin(1,4));
    end
    
    % ATTN: here, it is assumed that n_sp is one or only the fist one is
    % selected. In another words, there is no pathplanning or selection
    % strategy to choose one of the n_sp points!!!
    numX = mapSize(1,1)/cellSize;
    numY = mapSize(1,2)/cellSize;
    score = ones(numX, numY, 1);
    
    for k=1:1:maxItr
        %close all
        k
        selectedPos = [];
        selectedIdx = [];
        %MODEL Q: build model -------------------------------
        %subSimData = simData(X(:,1), :);
        [val id] = min(sqrt((simData(:,3) - pos(size(pos,1),1)).^2 + (simData(:,4) - pos(size(pos,1),2)).^2));
        subSimData = [subSimData; simData(id, :)];
        [ Q QVar predModel] = doBuildGDM(subSimData, time, mapCenter, mapSize);
        
        if (bVisualizePredModel)
            subplot(2,1,1)
            surfc(predModel(:,:,1), predModel(:,:,2), predModel(:,:,4), 'EdgeColor', 'none');
            view(2);
            subplot(2,1,2)
            surfc(predModel(:,:,1), predModel(:,:,2), predModel(:,:,5), 'EdgeColor', 'none');            
        end
        % %TODO ---------------------------
        % Remove the selected sensor from set of feasible anwers!!!!!!!
        % ---------------------------------
        % A = A(1, A(1,:) ~= X(1,:));
        % A(selectedSensor) = [];
        % ---------------------------------

        % KL divergence ========================================================
        Kullback_distance_mean = 0;
        Kullback_distance_var = 0;
        
        %Calculate KL-distance of the predictive model from the full model
        [Kullback_distance_mean Kullback_distance_var] = calculateKullbackLeiblerDistance(P, PVar, Q, QVar);
        klValue = [klValue; Kullback_distance_mean Kullback_distance_var];         
        combination = [coeff(1,1) coeff(1,2) coeff(1,3) coeff(1,4)];
        if (strcmp(samplingType, 'APF'))
            [selectedX selectedPos potential score] = ...
                doAPFSensorPlacement(score, simData, predModel, cellSize, mapSize, n_sp, ...
                maxRepuls, combination, pos, selectionType); %!!!!! COEFF
        elseif (strcmp(samplingType, 'random'))
            [val selectedX] = min(sqrt((simData(:,3) - rndList(k,1)).^2+ (simData(:,4) - rndList(k,2)).^2));
            selectedPos = [simData(selectedX,3) simData(selectedX,4)];
        end
        pos = [pos; selectedPos];
        X = [X; selectedX];      
        model = [model; predModel];
    end
    
    %% SAVE VARIABLES IN MAT ===============================================
    outputFile = [outputPath '/' experimentDescription '-' gdmConfig '-' spConfig];

    if (bSaveResult)
        klValueFile = [outputFile '-' 'KLValues' '.mat'];
        modelFile = [outputFile '-' 'Models' '.mat'];
        positionFile = [outputFile '-' 'Positions' '.mat'];
        samplesFile = [outputFile '-' 'SAMPLES' '.mat'];
        fulldataFile = [outputFile '-' 'FULLDATA' '.mat'];
        save(klValueFile, 'klValue');
        save(modelFile, 'model');
        save(positionFile, 'pos');      
        save(samplesFile, 'subSimData');
        save(fulldataFile, 'inputData');
    end
    
    %% SAVE SOURCE FILES ==================================================
    if (bSaveSrc)
       srcPath = [outputPath '/' 'src' '/'];
        mkdir(srcPath);
        copyfile('*.m', srcPath);
    end
    
    %% SAVE DESCRIPTION OF THE EXPERIMENT =================================
    if (bSaveDescription)
        descriptionFile = [outputFile '-' 'Description' '.txt'];   
        saveSensorPlanningDescription(descriptionFile, ...
            spConfig, maxRepuls, gdmConfig, maxItr, n_sp, samplingType, ...
            sensorsUsed, bNormalizeSensorData, sensorsUsedDescr, expClassDescr, ...
            experimentLabel, experimentSubDescr, logFileType, mapCenter, mapSize, srcLocation, ...
            initInd, endInd, sweepsInd, calibrationFile, time, margin, ...
            size(inputData,1));        
    end
    %% CLEAR LOCAL VARIABLE ===============================================
    clear klValue
    clear pos
    clear inputData
    clear subSimData
    clear model
    clear predModel
    clear X
end

function [sensorsUsed, bNormalizeSensorData, sensorsUsedDescr, expClassDescr] = selectSensors()

% %---------------------------------
% % Simulation Experiments (2010)
% %---------------------------------
bNormalizeSensorData = true; % set to 'true' if the sensors are not calibrated
expClassDescr = 'Simulation Experiments (Dec 2010)';
%Infact, no sensor is used in the simulation, however in the real windtunnel experiment 
%this kind of sensor was used.
sensorsUsed = 3; 
sensorsUsedDescr = 'TGS2620, Nose-A'; 

% %--------------------------------
% % 2D Experiments, Apr - Sep 2007
% %--------------------------------
% expClassDescr = '2D Experiments (Apr - Sep 2007)';
% sensorsUsed = 1; sensorsUsedDescr = 'TGS4161'; % doesn't work properly
% sensorsUsed = 2; sensorsUsedDescr = 'TGS2600_1';
% sensorsUsed = 3; sensorsUsedDescr = 'TGS2620';
% sensorsUsed = 4; sensorsUsedDescr = 'TGS2611';
% sensorsUsed = 5; sensorsUsedDescr = 'TGS2602';
% sensorsUsed = 6; sensorsUsedDescr = 'TGS2600_2';
% sensorsUsed = [2 6]; sensorsUsedDescr = 'both TGS2600 sensors';

%--------------------------
% 3D Experiments, Oct 2008
%--------------------------
%expClassDescr = '3D Experiments (Oct 2008)';
%-------------------
% Nose-A (h = 10cm)
%-------------------
% sensorsUsed = 1; sensorsUsedDescr = 'TGS4161, Nose-A'; % doesn't work properly
%sensorsUsed = 2; sensorsUsedDescr = 'TGS2600_1, Nose-A';
% sensorsUsed = 3; sensorsUsedDescr = 'TGS2620, Nose-A';
% sensorsUsed = 4; sensorsUsedDescr = 'TGS2611, Nose-A';
% sensorsUsed = 5; sensorsUsedDescr = 'TGS2602, Nose-A';
% sensorsUsed = 6; sensorsUsedDescr = 'TGS2600_2, Nose-A';
% sensorsUsed = [2 6]; sensorsUsedDescr = 'both TGS2600 sensors, Nose A';
%-------------------
% Nose-B (h = 60cm)
%-------------------
% sensorsUsed = 7; sensorsUsedDescr = 'TGS2620_1, Nose-B';
% sensorsUsed = 8; sensorsUsedDescr = 'TGS2620_2, Nose-B';
% sensorsUsed = 9; sensorsUsedDescr = 'TGS2620_3, Nose-B';
% sensorsUsed = 10; sensorsUsedDescr = 'TGS2620_4, Nose-B';
% sensorsUsed = [7 8 9 10]; sensorsUsedDescr = 'all TGS2620 sensors, Nose B'; 
%--------------------
% Nose-C (h = 110cm)
%--------------------
% sensorsUsed = 15; sensorsUsedDescr = 'TGS4161, Nose-C'; % doesn't work properly
% sensorsUsed = 16; sensorsUsedDescr = 'TGS2600_1, Nose-C';
% sensorsUsed = 17; sensorsUsedDescr = 'TGS2620, Nose-C';
% sensorsUsed = 18; sensorsUsedDescr = 'TGS2611, Nose-C';
% sensorsUsed = 19; sensorsUsedDescr = 'TGS2602, Nose-C';
% sensorsUsed = 20; sensorsUsedDescr = 'TGS2600_2, Nose-C';
% sensorsUsed = [16 20]; sensorsUsedDescr = 'both TGS2600 sensors, Nose C';
%----------------
% Pseudo-Sensors
%----------------
% sensorsUsed = 21; sensorsUsedDescr = 'wind-U';
% sensorsUsed = 22; sensorsUsedDescr = 'wind-V';
% sensorsUsed = 23; sensorsUsedDescr = 'wind-W'; bNormalizeSensorData = false;
% sensorsUsed = 24; sensorsUsedDescr = '2D wind strength (UV)'; bNormalizeSensorData = false;
% sensorsUsed = 25; sensorsUsedDescr = '3D wind strength (UVW)'; bNormalizeSensorData = false;
% sensorsUsed = 26; sensorsUsedDescr = 'temperature (Anemometer)'; bNormalizeSensorData = false;
% sensorsUsed = 27; sensorsUsedDescr = 'temperature, Nose-A'; bNormalizeSensorData = false;
% sensorsUsed = [26 27]; sensorsUsedDescr = 'temperature, Anemometer and Nose-A'; bNormalizeSensorData = false;
% sensorsUsed = [27 28]; sensorsUsedDescr = 'temperature and humidity, Nose-A'; bNormalizeSensorData = false;
% sensorsUsed = 28; sensorsUsedDescr = 'humidity, Nose-A'; bNormalizeSensorData = false;
% sensorsUsed = 29; sensorsUsedDescr = 'temperature, Nose-C';
% sensorsUsed = 30; sensorsUsedDescr = 'humidity, Nose-C';
%--------------
% Combinations
%--------------
% sensorsUsed = [3 7 17]; sensorsUsedDescr = 'TGS2620, Nose-A, Nose-B and Nose-C'; bNormalizeSensorData = false;
% sensorsUsed = [17 7 3]; sensorsUsedDescr = 'TGS2620, Nose-C, Nose-B and Nose-A'; bNormalizeSensorData = false;
% sensorsUsed = [3 17]; sensorsUsedDescr = 'TGS2620, Nose-A and Nose-C'; bNormalizeSensorData = false;
end
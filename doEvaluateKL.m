function [] = doEvaluateKL(experimentType, mapType, samplingMethod, dataType, inSpConfig, ...
            gdmConfig, experimentDescription, ...
            totalNumRun, totalRunSamples, ...
            bShowVisualize)
    bSweepV = false;
    bSweepH = false;
    bRand = true;
    bAPF = true;
    resultPath = ['result/' experimentType '/' samplingMethod];
    mkdir(resultPath);

    x = [1:1:totalRunSamples]';

    mKL_rnd = [];
    mKL_apf = [];
    mKL_sweep_h = [];
    mKL_sweep_v = [];
    
    vKL_rnd = [];
    vKL_apf = [];
    vKL_sweep_h = [];
    vKL_sweep_v = [];
    
    modelType = 'fulldata';
    
    spConfig_apf = inSpConfig; 
    spConfig_rnd = sprintf('M%.1f-V%.1f-R%.1f-C%.1f', 0, 0, 0, 0);
    
    if (bRand)
       dirNamePrefix = ['result/random']
       for itr=1:totalNumRun
           dirName = [dirNamePrefix '/' 'SP-' num2str(itr) '-' spConfig_rnd];
           fileName = [dirName '/' 'KL' '-' experimentType '-' 'filaments100' '_' modelType '-' ...
                     gdmConfig '-' spConfig_rnd '-' dataType '.mat'];
           kl_rnd = load(fileName);
           mKL_rnd = [mKL_rnd kl_rnd.klValue(:,1)];
           vKL_rnd = [vKL_rnd kl_rnd.klValue(:,2)]; 
       end        
       muM_rnd = mean(mKL_rnd, 2);
       sdM_rnd = std(mKL_rnd, 0, 2);
       muV_rnd = mean(vKL_rnd, 2);
       sdV_rnd = std(vKL_rnd, 0, 2);
    end

    if (bAPF)
       dirNamePrefix = ['result/'  'APF']
       for itr=1:totalNumRun
           dirName = [dirNamePrefix '/' 'SP-' num2str(itr) '-' spConfig_apf];
           fileName = [dirName '/' 'KL' '-' experimentType '-' 'filaments100' '_' modelType '-' ...
                     gdmConfig '-' spConfig_apf '-' dataType '.mat'];
           
           if (exist(fileName, 'file') == false)
               return
           end
           kl_apf = load(fileName);
           mKL_apf = [mKL_apf kl_apf.klValue(:,1)];
           vKL_apf = [vKL_apf kl_apf.klValue(:,2)]; 
       end        
       muM_apf = mean(mKL_apf, 2);
       sdM_apf = std(mKL_apf, 0, 2);
       muV_apf = mean(vKL_apf, 2);
       sdV_apf = std(vKL_apf, 0, 2);
    end
    
    if (bSweepV)
       dirNamePrefix = ['output/' experimentType '/' 'sweep-vertical']
       for itr=1:totalNumRun
           dirName = [dirNamePrefix '/' 'SP-' num2str(itr) '-' spConfig_rnd];
           fileName = [dirName '/' 'KL' '-' experimentType '-' 'filaments100' '_' modelType '-' ...
                     gdmConfig '-' spConfig_rnd '-' dataType '.mat'];
           kl_sweep_v = load(fileName);
           mKL_sweep_v = [mKL_sweep_v kl_sweep_v.klValue(:,1)];
           vKL_sweep_v = [vKL_sweep_v kl_sweep_v.klValue(:,2)]; 
       end        
       muM_sweep_v = mean(mKL_sweep_v, 2);
       sdM_sweep_v = std(mKL_sweep_v, 0, 2);
       muV_sweep_v = mean(vKL_sweep_v, 2);
       sdV_sweep_v = std(vKL_sweep_v, 0, 2);
    end
    
    if (bSweepH)
       dirNamePrefix = ['output/' experimentType '/' 'sweep-horizontal']
       for itr=1:totalNumRun
           dirName = [dirNamePrefix '/' 'SP-' num2str(itr) '-' spConfig_rnd];
           fileName = [dirName '/' 'KL' '-' experimentType '-' 'filaments100' '_' modelType '-' ...
                     gdmConfig '-' spConfig_rnd '-' dataType '.mat'];
           kl_sweep_h = load(fileName);
           mKL_sweep_h = [mKL_sweep_h kl_sweep_h.klValue(:,1)];
           vKL_sweep_h = [vKL_sweep_h kl_sweep_h.klValue(:,2)]; 
       end        
       muM_sweep_h = mean(mKL_sweep_h, 2);
       sdM_sweep_h = std(mKL_sweep_h, 0, 2);
       muV_sweep_h = mean(vKL_sweep_h, 2);
       sdV_sweep_h = std(vKL_sweep_h, 0, 2);
    end
    
    
    delay = 0;
    maxY = 2.5;  minY = 0;    %!!!
    maxX = totalRunSamples; minX = 1;    %!!!

    if (strcmp(mapType, 'variance'))
        col = 2;
        maxY = 7;%'variance'
    else
        col = 1; %'mean'
    end

    close all
    h = figure('Units', 'pixels', 'Position', [100 100 1400 800]);
    if bShowVisualize
        set(h, 'visible', 'on');
    else
        set(h, 'visible', 'off');
    end
    
    txtTitle = ({['KL-distance comparison of predictive ' mapType ' maps'] ...
        ['created by different sampling strategies (' experimentType 'experiment)']}); 
    hTitle = title(txtTitle);

    hXLabel = xlabel('sampling iteration');          
    hYLabel = ylabel('KL-distance value');

    %axis([minX+delay maxX minY maxY]);
    set(gca, 'LineWidth', 1);
    box on

    set( hTitle, 'FontSize', 9, 'FontWeight' , 'bold');
    set( hXLabel, 'FontSize', 7, 'FontWeight' , 'bold');
    set( hYLabel, 'FontSize', 7, 'FontWeight' , 'bold');

    lineKL_apf = [];
    lineKL_rnd = [];
    lineKL_sweep_v = [];
    lineKL_sweep_h = [];
    if (strcmp(mapType, 'mean'))      
        if (bAPF)
            lineKL_apf = line(x, muM_apf);
            set(lineKL_apf, 'Color',  [1 0 0], 'LineWidth', 1);
        end
        if (bRand)
            lineKL_rnd = line(x, muM_rnd);
            set(lineKL_rnd, 'Color',  [0 0 1], 'LineWidth', 1);
        end
        if (bSweepV)
            lineKL_sweep_v = line(x, muM_sweep_v);
            set(lineKL_sweep_v, 'Color',  [0 1 0], 'LineWidth', 1);
        end
        if (bSweepH)
            lineKL_sweep_h = line(x, muM_sweep_h);
            set(lineKL_sweep_h, 'Color',  [0 0.5 0.5], 'LineWidth', 1);
        end
    else
        if (bAPF)
            lineKL_apf = line(x, muV_apf);
            set(lineKL_apf, 'Color',  [1 0 0], 'LineWidth', 1);
        end
        if (bRand)
            lineKL_rnd = line(x, muV_rnd);
            set(lineKL_rnd, 'Color',  [0 0 1], 'LineWidth', 1);
        end
        if (bSweepV)
            lineKL_sweep_v = line(x, muV_sweep_v);
            set(lineKL_sweep_v, 'Color',  [0 1 0], 'LineWidth', 1);
        end
        if (bSweepH)
            lineKL_sweep_h = line(x, muV_sweep_h);
            set(lineKL_sweep_h, 'Color',  [0 0.4 0.4], 'LineWidth', 2);
        end
    end
    
    hold on 
    
%     if (bAPF)
%         if (strcmp(mapType, 'mean'))
%             fill([x; flipud(x)],[muM_apf + sdM_apf; flipud(muM_apf - sdM_apf)], 'r','EdgeColor','r','FaceAlpha',0.3,'EdgeAlpha',0.3, 'linestyle', '-.', 'linewidth', 1);   
%         else
%             fill([x; flipud(x)],[muV_apf + sdV_apf; flipud(muV_apf - sdV_apf)], 'r','EdgeColor','r','FaceAlpha',0.3,'EdgeAlpha',0.3, 'linestyle', '-.', 'linewidth', 1);   
%         end
%         hold on
%     end
%     if (bRand)
%         if (strcmp(mapType, 'mean'))
%             fill([x; flipud(x)],[muM_rnd + sdM_rnd; flipud(muM_rnd - sdM_rnd)], 'b','EdgeColor','b','FaceAlpha',0.3,'EdgeAlpha',0.3, 'linestyle', '-.', 'linewidth', 1);   
%         else
%             fill([x; flipud(x)],[muV_rnd + sdV_rnd; flipud(muV_rnd - sdV_rnd)], ...
%                 'b','EdgeColor','b','FaceAlpha',0.3,'EdgeAlpha',0.3, 'linestyle', '-.', 'linewidth', 1);   
%         end
%     end
% 
%     if (bSweepV)
%         if (strcmp(mapType, 'mean'))
%             fill([x;flipud(x)],[muM_sweep_v+sdM_sweep_v;flipud(muM_sweep_v-sdM_sweep_v)], 'g','EdgeColor','g','FaceAlpha',0.3,'EdgeAlpha',0.3, 'linestyle', '-.', 'linewidth', 1);   
%         else
%             fill([x;flipud(x)],[muV_sweep_v+sdV_sweep_v;flipud(muV_sweep_v-sdV_sweep_v)], ...
%                 'g','EdgeColor','g','FaceAlpha',0.3,'EdgeAlpha',0.3, 'linestyle', '-.', 'linewidth', 1);   
%         end
%     end
%     
%     if (bSweepH)
%         if (strcmp(mapType, 'mean'))
%             fill([x;flipud(x)],[muM_sweep_h+sdM_sweep_h;flipud(muM_sweep_h-sdM_sweep_h)], 'b','EdgeColor','b','FaceAlpha',0.2,'EdgeAlpha',0.2, 'linestyle', '-.', 'linewidth', 1);   
%         else
%             fill([x;flipud(x)],[muV_sweep_h+sdV_sweep_h;flipud(muV_sweep_h-sdV_sweep_h)], ...
%                 'b','EdgeColor','b','FaceAlpha',0.2,'EdgeAlpha',0.2, 'linestyle', '-.', 'linewidth', 1);   
%         end
%     end
    
    %set(gca,'FontSize',9);

    legStr_1 = sprintf('\\fontsize{9} APF-based sampling: ', spConfig_apf);
    legStr_rnd = '\fontsize{9} random sampling';
    legStr_sweep_h = '\fontsize{9} horizontal sweep sampling';
    legStr_sweep_v = '\fontsize{9} vertical sweep sampling';

    %%TODO
    hLegend = legend([lineKL_apf lineKL_rnd lineKL_sweep_h lineKL_sweep_v], ...
        legStr_1, legStr_rnd, legStr_sweep_h, legStr_sweep_v, 'location', 'NorthEast' );
    set(hLegend, 'LineWidth', 1);

    file = [resultPath '/' 'result-' num2str(delay) '-' experimentType '_' mapType '-' gdmConfig '-' spConfig_apf '-' 'confidenceInterval'];
    %hold off
    saveas(h, [file '.fig'], 'fig');
    print(h, [file '.png'], '-dpng', '-r300');
%     save([file '.mat'], 'muM_apf', 'sdM_apf', ...
%         'muM_rnd', 'sdM_rnd', 'muM_sweep_h', 'sdM_sweep_v', 'muM_sweep_v', 'sdM_sweep_v');    

end
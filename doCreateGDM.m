function [ ] = doCreateGDM( P, PVar, ...
    samplingMethod, selectionType, combination, ...
    samplingPoints, totalNumSamples, runNum,  ...
    experimentType, experimentLabel, gdmType, ...    
    inputFolder, resultFolder, ...
    bVisualizeData, bNormalizeData, bNormalizeTime)
%% Description


%% cofigure saving and visualization ==================================
    % in the next version I will remove these since, data will be saved in
    % mat files here and later, with others functions viualization will be
    % done.
    
    bVisualizeData = false;
    bVisualizePredModel = false; 
    bVisualizaKL = false;
    
    bSaveResult = true;
    bSaveDescription = true;
    bSaveSrc = false; %true;
    
    n_sp = 1;
    maxRepuls = 1;
    
    % select experiment corresponding to the input experimentLabel
    [experimentType, fileName, startInd, endInd, sweepsInd, ...
        mapCenter, mapSize, ...
        sourceLocation, gasType, experimentSubDescr] = selectExperiment(experimentLabel);
       
    % setting the indices to cut samples when extracting input data.
    % indices. This is only for the simulation experiments.
    startTime = 100;
    if strcmp(experimentType, 'simulation_2d')
        endTime = 20;
    else
        endTime = 100;
    end
    
    time = endTime;
    
    % extract input data
    simData = extractInputData(inputFolder, experimentType, ...
        experimentLabel, fileName, startTime, endTime, startInd, endInd, ...
        bNormalizeData, bNormalizeTime, bVisualizeData);
    
    if strcmp(experimentType, 'simulation_ros')
        simData(:,7) = simData(:,3)/0.25;
        simData(:,8) = simData(:,4)/0.25;
    end    

    % select GDM
    [kernelWidth, cellSize, timeScale] = selectMetaParameters(experimentLabel);
    %[kernelWidth, cellSize, timescale] = [0.25 1.25 0.00];


        %% set output path ====================================================
    %%coeff = [0.1 0.1 0.8]
    
    msgRunNum = num2str(runNum);
    gdmConfig = sprintf('c%.2f-s%.2f', cellSize, kernelWidth);
    spConfig = sprintf('M%.1f-V%.1f-R%.1f-C%.1f', ...
        combination(1,1), combination(1,2), combination(1,3), combination(1,4)); 
    
    %outputPath = ['output/' 'SensorPlanning-' msgDate '-' spConfig];
    outputPath = [resultFolder '/' samplingMethod '/SP-' msgRunNum '-' spConfig];
    if ~exist(outputPath, 'dir')
        mkdir(outputPath);

        experimentDescription = ['KL-' experimentType '-filaments100_fulldata'];
        preOutputFile = [outputPath '/' experimentDescription '-' gdmConfig];        

        %% SELECT A RANDOM INITIAL POSITION ===================================
        X = [];
        pos = [];
        model = [];
        samples = zeros(totalNumSamples, 3); %[x y r]
        klValue = [];

        %% Extract pre selected sampling points
        if (~strcmp(samplingMethod, 'APF'))
            for i=1:totalNumSamples
                [val, selectedX] = min(sqrt((simData(:,3) - samplingPoints(i,1)).^2+ (simData(:,4) - samplingPoints(i,2)).^2));
                samples(i, :) = [simData(selectedX,3) simData(selectedX,4) simData(selectedX,2)]; 
            end
            pos = [pos; samples(1,1:2)];
            selectedSensor = find((simData(:,3) == samples(1,1)) & (simData(:,4) == samples(1,2)));
        else
            %for now, the initial sensor id is selected randomly
            RandStream.setGlobalStream ...
                 (RandStream('mt19937ar','seed',sum(100*clock)));

            rd = randi(size(simData,1)); %received as input
            selectedIdx = rd;    
            pos =  [pos; simData(selectedIdx,3) simData(selectedIdx,4)];
            selectedSensor = find((simData(:,7) == simData(selectedIdx,7)) & (simData(:,8) == simData(selectedIdx,8)));

        end

        X = [X; selectedSensor];
        subSimData = simData(X(:,1), :);

        %% ATTN: here, it is assumed that n_sp is one or only the fist one is
        % selected. In another words, there is no pathplanning or selection
        % strategy to choose one of the n_sp points!!!
        numX = mapSize(1,1)/cellSize;
        numY = mapSize(1,2)/cellSize;
        score = ones(numX, numY, 1);

        %% RUN SAMPLING ITERATIONS
        for k=1:1:totalNumSamples
            %close all
            k
            %MODEL Q: build model -------------------------------
            %subSimData = simData(X(:,1), :);
            [val, id] = min(sqrt((simData(:,3) - pos(size(pos,1),1)).^2 + (simData(:,4) - pos(size(pos,1),2)).^2));

            % added to make sure first iteration is only one sample
            if (k > 1)
               subSimData = [subSimData; simData(id, :)];
            end
            [ Q, QVar, predModel] = doBuildGDM(subSimData, time, mapCenter, mapSize, experimentLabel);

            if (bVisualizePredModel)
                subplot(2,1,1)
                surfc(predModel(:,:,1), predModel(:,:,2), predModel(:,:,4), 'EdgeColor', 'none');
                view(2);
                subplot(2,1,2)
                surfc(predModel(:,:,1), predModel(:,:,2), predModel(:,:,5), 'EdgeColor', 'none');            
                view(2);
            end
            % %TODO ---------------------------
            % Remove the selected sensor from set of feasible anwers!!!!!!!
            % ---------------------------------
            % A = A(1, A(1,:) ~= X(1,:));
            % A(selectedSensor) = [];
            % ---------------------------------

            % KL divergence ========================================================
            Kullback_distance_mean = 0;
            Kullback_distance_var = 0;

            %Calculate KL-distance of the predictive model from the full model
            [Kullback_distance_mean, Kullback_distance_var] = calculateKullbackLeiblerDistance(P, PVar, Q, QVar);
            klValue = [klValue; Kullback_distance_mean Kullback_distance_var];         

            if (strcmp(samplingMethod, 'APF'))
                [selectedX, selectedPos, potential, score] = ...
                    doAPFSensorPlacement(score, simData, predModel, cellSize, mapSize, n_sp, ...
                    maxRepuls, combination, pos, selectionType); %!!!!! COEFF
            else
                selectedPos = samples(k,1:2);
            end
            pos = [pos; selectedPos];
            X = [X; selectedX];      
            model = [model; predModel];

            clear predModel
        end

        %% SAVE VARIABLES IN MAT ===============================================
        outputFile = [outputPath '/' experimentDescription '-' gdmConfig '-' spConfig];
        figure; plot(pos(:,1), pos(:,2), '--rx')
        if (bSaveResult)
            klValueFile = [outputFile '-' 'KLValues' '.mat'];
            modelFile = [outputFile '-' 'Models' '.mat'];
            positionFile = [outputFile '-' 'Positions' '.mat'];
            samplesFile = [outputFile '-' 'SAMPLES' '.mat'];
            save(klValueFile, 'klValue');
            %save(modelFile, 'model');
            save(positionFile, 'pos');      
            save(samplesFile, 'subSimData');
        end

        %% SAVE SOURCE FILES ==================================================
        if (bSaveSrc)
            srcPath = [outputPath '/' 'src' '/'];
            mkdir(srcPath);
            copyfile('*.m', srcPath);
        end

        %% SAVE DESCRIPTION OF THE EXPERIMENT =================================
        calibrationFile = '';
        margin = 1;
        
        if (bSaveDescription)
            descriptionFile = [outputFile '-' 'Description' '.txt'];   
            saveSensorPlanningDescription(descriptionFile, ...
                spConfig, maxRepuls, gdmConfig, totalNumSamples, n_sp, samplingMethod, ...
                experimentLabel, mapCenter, mapSize, sourceLocation, ...
                startInd, endInd, sweepsInd, calibrationFile, time, margin, ...
                experimentSubDescr, ...
                size(simData,1));        
        end
        %% CLEAR LOCAL VARIABLE ===============================================
        clear klValue
        clear pos
        clear inputData
        clear subSimData
        clear model
        clear predModel
        clear X
    end
end


#!/bin/sh

MATHLAB_PATH=/Applications/MATLAB/MATLAB_R2015b.app/bin/matlab
PROGRAM="callSensorPlanning"
OUTPUT="output"

killall -9 MATLAB

${MATHLAB_PATH} -nodisplay -nosplash -r ${PROGRAM}
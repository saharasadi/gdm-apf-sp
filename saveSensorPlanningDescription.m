function [] = saveSensorPlanningDescription(descriptionFile, ...
            spConfig, maxRepuls, gdmConfig, maxItr, n_sp, samplingType, ...
            experimentLabel, mapCenter, mapSize, srcLocation, ...
            initInd, endInd, sweepsInd, calibrationFile, time, margin, ...
            experimentSubDescr, ...
            dataSize)
        
    msgAPF = [' --> APF coeffiction: ' spConfig '\n' '\t\tmaxRepuls: ' num2str(maxRepuls) '\n'];
    msgSampling = [' --> Sampling type: ' samplingType '\n'];
    msgGDM = [' --> GDM config: ' gdmConfig '\n'];
    
%     msgSensor = ['\n\tsensorsUsed: ' num2str(sensorsUsed) '\n\tNormalization: ' num2str(bNormalizeSensorData) ...
%         '\n\tsensorsUsedDescr'  sensorsUsedDescr '\n\texpClassDescr' expClassDescr];
%     
    msgExperiment = ['\n\tExperimentLabel: ' experimentLabel '\n\texperimentSubDescr:' experimentSubDescr ...
        '\n\tmapCenter: ' num2str(mapCenter) '\n\tmapSize: ' num2str(mapSize) ...
        '\n\tsrcLocation: ' num2str(srcLocation) ...
        '\n\tsrcLocation: ' num2str(initInd) '\n\tendInd: ' num2str(endInd) '\n\tsweepInd: ' num2str(sweepsInd) ...
        '\n\tcalibrationFile' calibrationFile '\n\ttime: ' num2str(time) '\n\tmargin: ' num2str(margin)];
    
    msgInput = ['\n\tsize of input Data: ' num2str(dataSize)];
    
    fid = fopen(descriptionFile, 'w'); 
    fprintf(fid, '\n=======================================================\n\t\t');
    fprintf(fid, datestr(now));
    fprintf(fid, '\n=======================================================\n\n');
    fprintf(fid, '\ttotal iteration number: %d\n\tnumber of suggested points at each iteration: %d\n\n', ...
        maxItr, n_sp);
    fprintf(fid, '\n-------------------------------------------------------\n');
    fprintf(fid, msgGDM);
    fprintf(fid, msgAPF);
    fprintf(fid, '\n-------------------------------------------------------\n');
%     fprintf(fid, msgSensor);
%     fprintf(fid, '\n-------------------------------------------------------\n');
    fprintf(fid, msgExperiment);
    fprintf(fid, '\n\n=======================================================\n');
    fprintf(fid, '\n\tEND\n');
    fprintf(fid, '\n=======================================================\n');
    fclose(fid);
    
end
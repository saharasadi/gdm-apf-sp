function convertToPNG(outputPath)

listing = dir(fullfile([outputPath '*.fig']));

for i = 1 : size(listing)
    file = [outputPath listing(i).name];
    h = open(file);
    
    file = strrep(file, '.fig', '.png');
    print(h, '-dpng', '-r300', file);  
end

function [inputData] = extractInputDataFromSimulationROS(inputFolder, fileName)

% Load input data from file
inputDataRaw = load([inputFolder '/' fileName '.mat']);

inputData = inputDataRaw.inputData;
% Sort inputData based on the sampling time
inputData = sortrows(inputData, 1);

end

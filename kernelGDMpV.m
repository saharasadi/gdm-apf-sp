function [kGdmvMap, ...
    kGdmWeightMin, kGdmWeightMax, ...
    kGdmMapMin, kGdmMapMax, totalMean, ...
    kGdmVarMin, kGdmVarMax, sigma2Tot] ...
    = ...
    kernelGDMpV(inputData, trainingSetIndices, ...
    mapGeometry, ...
    kernelWidth, maxKernelEvalRadius, weightVarianceSteepness, ...
    bVerbose)
% This function is an implementation of the Kernel GDM+V algorithm, which
% can be used for (but is not limited to) gas distribution mapping. 
%
% A call must specify the following parameters 
% - inputData
%   The input data in the form [tEN r1 x1 y1 r2 x2 y2 ...] where tEN is a
%   column vector of the measurement time, ri (xi yi) are column vectors
%   composed of the readings and the measurement for sensor i locations
%   respectively.
% - trainingSetIndices
%   Indices of the data points used to build the model. Together, inputData
%   and trainingSetIndices determine which samples are used for training.
%   (At the moment only measurement times can be selected - but not
%   individual sensors.)
% - mapGeometry
%   Structure defining the number of cells (numOfMapCells), the location
%   represented by the center in the map in real world coordinates
%   (mapCenter), the real word size of cells (mapCellSize) and the real
%   world coordinated of the lower left corner of the map (mapLowerLeft).
%   All variables are 2-d vectors, describing the respective property in x-
%   and y-direction. The parameters mapCenter and mapLowerLeft both
%   determine the range of real world coordinates that is represented by
%   the gas distribution map. The resolution of the map is defined by
%   mapCellSize and numOfMapCells. These two pairs of variables are used in
%   a redundant way to avoid that they have to be computed each time this
%   function is called. It is assumed that they are consistent. This can be
%   achieved by using the function getMapGeometry.
% - kernelWidth
%   This is the parameter \sigma of the Kernel GDM+V algorithm.
% - maxKernelEvalRadius
%   This parameter determines at which distance from a point of measurement
%   the Kernel is not evaluated for efficiency reasons. It can be set to 
%   3 \sigma, for example.
% - weightVarianceSteepness
%   This parameter corresponds to the parameter \sigma_{Omega}^2 of the
%   Kernel GDM+V algorithm.
% - bVerbose
%   If set to "true", the algorithm will be carried outin verbose mode,
%   i.e. temporary results will be reported. For batch processing it should
%   be set to "false".
%
% The kernelGDMpV function returns the following variables
% - kGdmvMap
%   A NxMx6 dimensional matrix that represents the computed map with N, M
%   being the map size in x- and y-direction. The 6 dimensions represent:
%   iCellX,iCellY,1 -> x
%   iCellX,iCellY,2 -> y
%   iCellX,iCellY,3 -> integrated weights  (\Omega^{(k)})
%   iCellX,iCellY,4 -> mean estimates      (r^{(k)})
%   iCellX,iCellY,5 -> variance estimates  (v^{(k)})
%   iCellX,iCellY,6 -> inverse confidence  (\beta^{(k)} = 1 - \alpha^{(k)})
%
% For convenience, the algorithm also provides the following results
% - kGdmWeightMin
%   Minimum in the map of integrated weights.
% - kGdmWeightMax
%   Maximum in the map of integrated weights.
% - kGdmMapMin
%   Minimum in the map of mean estimates.
% - kGdmMapMax
%   Maximum in the map of mean estimates.
% - totalMean
%   Mean over all sensor values used to compute the map.
% - kGdmVarMin
%   Minimum in the map of variance estimates.
% - kGdmVarMax
%   Maximum in the map of variance estimates.
% - sigma2Tot
%   Variance over all sensor values used to compute the map.
%
% Kernel GDM+V Algorithm (v1.2.6)
%

%--------------------------------------------------------------------------
% (0) Compute constants (to improve efficiency)
gaussianFactor = 1 / ( sqrt(2.0 * pi) * kernelWidth); % faster than calling an external function  (approx. twice as fast!)
gaussianExpFactor = - 1 / (2.0 * kernelWidth * kernelWidth);
% NOTE: The first two computations can be done outside the main loop if the kernel is of constant size
maxKernelEvalRadius2 = maxKernelEvalRadius * maxKernelEvalRadius;


%--------------------------------------------------------------------------
% (1) Init
% init gas distribution map
mapCellsX = mapGeometry(1,1); % here it is assumed that mapCellsX is an integer, otherwise use "sign(mapGeometry(1,1))*ceil(abs(mapGeometry(1,1)));"
mapCellsY = mapGeometry(1,2); % here it is assumed that mapCellsY is an integer, otherwise use "sign(mapGeometry(1,2))*ceil(abs(mapGeometry(1,2)));"
kGdmvMap = zeros(mapCellsX,mapCellsY,6);

% set (x,y) values in the map
%mapCenterX = mapGeometry(1,3);
%mapCenterY = mapGeometry(1,4);
mapCellSizeX = mapGeometry(1,5); 
mapCellSizeY = mapGeometry(1,6);
mapLowerLeftX = mapGeometry(1,7);
mapLowerLeftY = mapGeometry(1,8);
[kGdmvMap(:,:,1), kGdmvMap(:,:,2)] = ndgrid(...
    mapLowerLeftX+0.5*mapCellSizeX:mapCellSizeX:mapLowerLeftX+mapCellsX*mapCellSizeX, ...
    mapLowerLeftY+0.5*mapCellSizeY:mapCellSizeY:mapLowerLeftY+mapCellsY*mapCellSizeY);
% use 'ndgrid' instead of 'meshgrid' (where one would have to refer to
% cells as (m, n) instead of (n, m) without additional transpose operations) 

% determine number of sensors to be used
numOfSensors = 1; %floor((size(inputData,2) - 1)/3); % works for inputData structure (tEN r1 x1 y1 r2 x2 y2 ... rN xN yN P1 P2) with up to two additional entries P1, P2
showDataPointNumInterval = 500 * numOfSensors;


%--------------------------------------------------------------------------
% (2) Integration of Weights and Weighted Readings over all Data Points
totalMean = 0.0;
totalR2 = 0.0;
nCount = 0;
if bVerbose == true
    fprintf('\nIntegration of weights and weighted readings ...\n');
end
for nCurrDataPoint = trainingSetIndices    
    if bVerbose == true
        if mod(nCount,showDataPointNumInterval) == 0
            fprintf('Data points processed: %i, current sample: %i ...\n', nCount, nCurrDataPoint-1);
        end
        nCount = nCount + numOfSensors;
    end
    for nSensor = 1:1:numOfSensors
        % Read values for the current data point
        r = inputData(nCurrDataPoint, 2 + (nSensor - 1) * 3 + 0);
        x = inputData(nCurrDataPoint, 2 + (nSensor - 1) * 3 + 1);
        y = inputData(nCurrDataPoint, 2 + (nSensor - 1) * 3 + 2);        
        totalMean = totalMean + r;
        totalR2 = totalR2 + r * r;
        %nDataPoints = nDataPoints + 1;
        %fprintf('Map value %i of sensor %i at (%5.2f,%5.2f) with value %.3f\n',nCurrDataPoint, nSensor, x, y, r);
        
        % Determine area that contains all cells which may be affected by the currently processed sensor reading
        [ minCoordsX, minCoordsY ] = gridCoordinates(x - maxKernelEvalRadius,y - maxKernelEvalRadius,mapGeometry,false);
        [ maxCoordsX, maxCoordsY ] = gridCoordinates(x + maxKernelEvalRadius,y + maxKernelEvalRadius,mapGeometry,false);
        %fprintf('Consider area from (%i,%i) to (%i,%i)\n',minCoordsX,minCoordsY,maxCoordsX,maxCoordsY);

        % REMOVE - if the corresponding issue is fixed
        if(minCoordsX < 0 || minCoordsY < 0)
            fprintf('gridCoordinates called with x = %f, y = %f, maxKernelEvalRadius = %f, cellSize = (%f,%f), lowerLeft = (%f,%f)\n', ...
                x,y, maxKernelEvalRadius, ...
                mapGeometry(1,5), mapGeometry(1,6), mapGeometry(1,7), mapGeometry(1,8));
            return;
        end
        % Check all these cells whether they are in the evaluation circle
        for iX = minCoordsX:1:maxCoordsX
            for iY = minCoordsY:1:maxCoordsY
                distCellCenter2MeasLocation2 = (kGdmvMap(iX,iY,1) - x)^2 + (kGdmvMap(iX,iY,2) - y)^2; % much faster than using "norm"
                %distCellCenter2MeasLocation = norm([kGdmvMap(iX,iY,1) kGdmvMap(iX,iY,2)] - [x y]);
                %fprintf('The cell (%i,%i) with centre at (%5.2f,%5.2f) has distance %5.2f to the data point at (%5.2f,%5.2f)\n',iX,iY,kGdmvMap(iX,iY,1),kGdmvMap(iX,iY,2),distCellCenter2MeasLocation,x,y);
                
                % Evaluate all the remaining cells
                if distCellCenter2MeasLocation2 < maxKernelEvalRadius2
                    %weight = normGaussian(kernelWidth,distCellCenter2MeasLocation); % using normGausian is approx. 5 times faster than using normpdf
                    weight = gaussianFactor * exp(gaussianExpFactor * distCellCenter2MeasLocation2); % faster than calling an external function  (approx. twice as fast!)
                    %fprintf('The cell (%i,%i) with centre at (%5.2f,%5.2f) has distance %5.2f to the data point at (%5.2f,%5.2f) and accordingly the weight %8.5f\n',iX,iY,kGdmvMap(iX,iY,1),kGdmvMap(iX,iY,2),distCellCenter2MeasLocation,x,y,weight);
                    kGdmvMap(iX,iY,3) = kGdmvMap(iX,iY,3) + weight;
                    kGdmvMap(iX,iY,4) = kGdmvMap(iX,iY,4) + weight * r;
                    if (r < 0)
                        fprintf('here r has negative value!\n');
                    end
                    if (kGdmvMap(iX,iY,3)<0 || kGdmvMap(iX,iY,4) < 0)
                        fprintf('here it is the negative value!\n');
                    end
                end
            end
        end
    end
end
nDataPoints = size(trainingSetIndices,2) * numOfSensors;
totalMean = min(inputData(:,2)); %totalMean / nDataPoints;
totalR2 = totalR2 / nDataPoints;
% sigma2Tot = totalR2 - totalMean * totalMean; % PREVIOUSLY USED - replaced by an estimate of sigma2Tot from the variance contributions
%fprintf('Total mean = %.4d\n',totalMean);
if bVerbose == true
    fprintf('Done!\n');
end
% Result from this step:
% -> kGdmvMap(iX,iY,3) (= \Omega^{(k)})
% -> kGdmvMap(iX,iY,4) (= R^{(k)})
% -> totalMean (= \bar{r})
% -> totalR2 (= \bar{r^2})
% -> sigma2Tot (= \sigma^2_{tot}) % PREVIOUSLY USED - replaced by an estimate of sigma2Tot from the variance contributions


%--------------------------------------------------------------------------
% (3) Computation of the Mean Estimates and the Inverse Confidence Map
if bVerbose == true
    fprintf('\nComputation of the inverse confidence map and the mean estimates ...\n');
end
kGdmWeightMin = Inf;
kGdmWeightMax = -Inf;
kGdmMapMin = Inf;
kGdmMapMax = -Inf;
for iX = 1:1:mapCellsX
    for iY = 1:1:mapCellsY
        totalWeight = kGdmvMap(iX,iY,3);
        totalWeightedReading = kGdmvMap(iX,iY,4);
        invConfidence = exp(-(totalWeight * totalWeight) / weightVarianceSteepness);
        kGdmvMap(iX,iY,6) = invConfidence;
        if totalWeight > 0
            kGdmvMap(iX,iY,4) = (1 - invConfidence) * (totalWeightedReading / totalWeight) + invConfidence * totalMean;
        else
            kGdmvMap(iX,iY,4) = totalMean;
        end

        % remember min/max values of total weight and weighted readings
        if kGdmvMap(iX,iY,3) > kGdmWeightMax
            kGdmWeightMax = kGdmvMap(iX,iY,3);
        end
        if kGdmvMap(iX,iY,3) < kGdmWeightMin
            kGdmWeightMin = kGdmvMap(iX,iY,3);
        end
        if kGdmvMap(iX,iY,4) > kGdmMapMax
            kGdmMapMax = kGdmvMap(iX,iY,4);
        end
        if kGdmvMap(iX,iY,4) < kGdmMapMin
            kGdmMapMin = kGdmvMap(iX,iY,4);
        end
    end
end
if bVerbose == true
    fprintf('Done!\n');
end
% Result from this step:
% -> kGdmvMap(iX,iY,6) (= \beta^{(k)})
% -> kGdmvMap(iX,iY,4) (= r^{(k)})


%--------------------------------------------------------------------------
% (4) Integration of Weighted Variance Contributions
totalVarContr = 0.0;
numOfVarContr = 0;
if bVerbose == true
   nCount = 0;
   fprintf('\nIntegration of the weighted variance contributions ...\n');
end
for nCurrDataPoint = trainingSetIndices    
    if bVerbose == true
        if mod(nCount,showDataPointNumInterval) == 0
            fprintf('Data points processed: %04i, current sample: %i ...\n', nCount, nCurrDataPoint-1);
        end
        nCount = nCount + numOfSensors;
    end
    for nSensor = 1:1:numOfSensors
        
        % Get values for the current data point
        r = inputData(nCurrDataPoint, 2 + (nSensor - 1) * 3 + 0);
        x = inputData(nCurrDataPoint, 2 + (nSensor - 1) * 3 + 1);
        y = inputData(nCurrDataPoint, 2 + (nSensor - 1) * 3 + 2);
        
        % determine corresponding predicted value
        [ cellX, cellY ] = gridCoordinates(x,y,mapGeometry,false); % returns always a map cell if last argument is false 
        predR = kGdmvMap(cellX,cellY,4);
        deltaR = r - predR;
        totalVarContr = totalVarContr + deltaR * deltaR;
        numOfVarContr = numOfVarContr + 1;

        % Determine area that contains all cells which may be affected by the currently processed sensor reading
        [ minCoordsX, minCoordsY ] = gridCoordinates(x - maxKernelEvalRadius,y - maxKernelEvalRadius,mapGeometry,false);
        [ maxCoordsX, maxCoordsY ] = gridCoordinates(x + maxKernelEvalRadius,y + maxKernelEvalRadius,mapGeometry,false);
        %fprintf('Consider area from (%i,%i) to (%i,%i)\n',minCoordsX,minCoordsY,maxCoordsX,maxCoordsY);
        
        % Check all these cells whether they are in the evaluation circle
        for iX = minCoordsX:1:maxCoordsX
            for iY = minCoordsY:1:maxCoordsY
                distCellCenter2MeasLocation2 = (kGdmvMap(iX,iY,1) - x)^2 + (kGdmvMap(iX,iY,2) - y)^2; % much faster than using "norm"
                %distCellCenter2MeasLocation = norm([kGdmvMap(iX,iY,1) kGdmvMap(iX,iY,2)] - [x y]);
                %fprintf('The cell (%i,%i) with centre at (%5.2f,%5.2f) has distance %5.2f to the data point at (%5.2f,%5.2f)\n',iX,iY,kGdmvMap(iX,iY,1),kGdmvMap(iX,iY,2),distCellCenter2MeasLocation,x,y);
                
                % Evaluate all the remaining cells
                if distCellCenter2MeasLocation2 < maxKernelEvalRadius2
                    weight = gaussianFactor * exp(gaussianExpFactor * distCellCenter2MeasLocation2); % faster than calling an external function  (approx. twice as fast!)
                    %weight = normGaussian(kernelWidth,distCellCenter2MeasLocation);
                    %fprintf('The cell (%i,%i) with centre at (%5.2f,%5.2f) has distance %5.2f to the data point at (%5.2f,%5.2f) and accordingly the weight %8.5f\n',iX,iY,kGdmvMap(iX,iY,1),kGdmvMap(iX,iY,2),distCellCenter2MeasLocation,x,y,weight);
                    kGdmvMap(iX,iY,5) = kGdmvMap(iX,iY,5) + weight * deltaR * deltaR;
                end
            end
        end
    end
end
if bVerbose == true
    fprintf('Done!\n');
end
sigma2Tot =  sqrt(totalVarContr)/numOfVarContr;
% Result from this step:
% -> kGdmvMap(iX,iY,4) (= V^{(k)})
% -> sigma2Tot (= \sigma^2_{tot})


%--------------------------------------------------------------------------
% (5) Computation of the Variance Estimates
if bVerbose == true
    fprintf('\nComputation of the variance estimates ...\n');
end
kGdmVarMin = Inf;
kGdmVarMax = -Inf;
for iX = 1:1:mapCellsX
    for iY = 1:1:mapCellsY
        totalWeight = kGdmvMap(iX,iY,3);
        totalWeightedVar = kGdmvMap(iX,iY,5);
        invConfidence = kGdmvMap(iX,iY,6);
        if totalWeight > 0
            kGdmvMap(iX,iY,5) = (1 - invConfidence) * (totalWeightedVar / totalWeight) + invConfidence * sigma2Tot;
        else
            kGdmvMap(iX,iY,5) = sigma2Tot;
        end

        % remember min/max values of total weight and weighted readings
        if kGdmvMap(iX,iY,5) > kGdmVarMax
            kGdmVarMax = kGdmvMap(iX,iY,5);
        end
        if kGdmvMap(iX,iY,5) < kGdmVarMin
            kGdmVarMin = kGdmvMap(iX,iY,5);
        end
    end
end
if bVerbose == true
    fprintf('Done!\n');
end
% Result from this step:
% -> kGdmvMap(iX,iY,5) (= v^{(k)})

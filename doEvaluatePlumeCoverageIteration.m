function [ ] = doEvaluatePlumeCoverageIteration( experimentType, mapType, samplingMethod, dataType, inSpConfig, ...
            gdmConfig, experimentDescription, totalNumRun, totalNumSamples, ...
            bShowVisualize)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

    bSweepV = true;
    bSweepH = true;
    bRand = true;
    bAPF = true;
    resultPath = ['result/' experimentType '/' samplingMethod];
    mkdir(resultPath);
    
    modelType = 'fulldata';
    spConfig_apf = inSpConfig; 
    spConfig_rnd = sprintf('M%.1f-V%.1f-R%.1f-C%.1f', 0, 0, 0, 0);
    
    avgPlume_sweep_v = zeros(1, 1);
    avgPlume_sweep_h = zeros(1, 1);
    avgPlume_rnd = zeros(1, 1);
    avgPlume_apf = zeros(1, 1);
    
    mean_sweep_v = zeros(1, 1);
    mean_sweep_h = zeros(1, 1);
    mean_rnd = zeros(1, 1);
    mean_apf = zeros(1, 1);
    
    std_sweep_v = zeros(1, 1);
    std_sweep_h = zeros(1, 1);
    std_rnd = zeros(1, 1);
    std_apf = zeros(1, 1);

    count_rnd = zeros(totalNumRun, 1);
    count_apf = zeros(totalNumRun, 1);
    count_sweep_h = zeros(totalNumRun, 1);
    count_sweep_v = zeros(totalNumRun, 1);   
       
    if (bRand)
       dirNamePrefix = ['output/' experimentType '/' 'random'];
       
       for itr=1:1:totalNumRun
           clear data
           clear measurement
           dirName = [dirNamePrefix '/' 'SP-' num2str(itr) '-' spConfig_rnd];
           fileName = [dirName '/' 'KL' '-' experimentType '-' 'filaments100' '_' modelType '-' ...
               gdmConfig '-' spConfig_rnd '-' dataType '.mat'];
           data = load(fileName);       
           measurement = data.subSimData(:,2);           
           count_rnd(itr,1) = size(find(measurement > 0.01), 1);
       end
       mean_rnd = mean(count_rnd, 1);
       std_rnd = std(count_rnd, 0);
       avgPlume_rnd = (1/totalNumRun) .* sum(count_rnd(:,1));
    end

    if (bAPF)
       dirNamePrefix = ['output/' experimentType '/' 'APF'];
        for itr=1:1:totalNumRun
            clear data
            clear measurement
           dirName = [dirNamePrefix '/' 'SP-' num2str(itr) '-' spConfig_apf];
           fileName = [dirName '/' 'KL' '-' experimentType '-' 'filaments100' '_' modelType '-' ...
               gdmConfig '-' spConfig_apf '-' dataType '.mat'];
           if (exist(fileName, 'file') == false)
               return
           end
           data = load(fileName);       
           measurement = data.subSimData(:, 2);           
           count_apf(itr,1) = size(find(measurement > 0.01), 1);
       end
       mean_apf = mean(count_apf);
       std_apf = std(count_apf, 0);
       avgPlume_apf = (1/totalNumRun) .* sum(count_apf(:,1));
    end

    if (bSweepH)
       dirNamePrefix = ['output/' experimentType '/' 'sweep-horizontal'];    
       for itr=1:1:totalNumRun
            clear data
            clear measurement
           dirName = [dirNamePrefix '/' 'SP-' num2str(itr) '-' spConfig_rnd];
           fileName = [dirName '/' 'KL' '-' experimentType '-' 'filaments100' '_' modelType '-' ...
               gdmConfig '-' spConfig_rnd '-' dataType '.mat'];
           data = load(fileName);       
           measurement = data.subSimData(:, 2);            
           count_sweep_h(itr,1) = size(find(measurement > 0.01), 1);
       end
       mean_sweep_h = mean(count_sweep_h);
       std_sweep_h = std(count_sweep_h, 0);
       avgPlume_sweep_h = (1/totalNumRun) .* sum(count_sweep_h(:,1));
    end

    if (bSweepV)
       dirNamePrefix = ['output/' experimentType '/' 'sweep-vertical'];
       for itr=1:1:totalNumRun
            clear data
            clear measurement
           dirName = [dirNamePrefix '/' 'SP-' num2str(itr) '-' spConfig_rnd];
           fileName = [dirName '/' 'KL' '-' experimentType '-' 'filaments100' '_' modelType '-' ...
               gdmConfig '-' spConfig_rnd '-' dataType '.mat'];
           data = load(fileName);       
           measurement = data.subSimData(:, 2);            
           count_sweep_v(itr,1) = size(find(measurement > 0.01), 1);
       end
       mean_sweep_v = mean(count_sweep_v);
       std_sweep_v = std(count_sweep_v, 0);
       avgPlume_sweep_v = (1/totalNumRun) .* sum(count_sweep_v(:,1));
    end
   
    
    close all;
    h = figure('Units', 'pixels', 'Position', [100 100 1400 800]);
    if bShowVisualize
        set(h, 'visible', 'on');
    else
        set(h, 'visible', 'off');
    end
    
    txtTitle = ({['Plume Coverage in each run ' mapType ' maps'] ...
        ['created by different sampling strategies (' experimentType 'experiment)']}); 
    hTitle = title(txtTitle);

    hXLabel = xlabel('Run number');          
    hYLabel = ylabel('Number of Samples with gas measrument > 0.01');

    set(gca, 'LineWidth', 1);
    box on

    set( hTitle, 'FontSize', 14, 'FontWeight' , 'bold');
    set( hXLabel, 'FontSize', 12, 'FontWeight' , 'bold');
    set( hYLabel, 'FontSize', 12, 'FontWeight' , 'bold');

    x = [1:totalNumRun]';
    linePlume_apf = [];
    linePlume_rnd = [];
    linePlume_sweep_v = [];
    linePlume_sweep_h = [];
    hold on
    axis([0 totalNumRun 0 totalNumSamples]);
    
    if (bAPF)
        linePlume_apf = line(x, count_apf);
        set(linePlume_apf, 'Color',  [1 0 0], 'LineWidth', 2);
    end
    if (bRand)
        linePlume_rnd = line(x, count_rnd);
        set(linePlume_rnd, 'Color',  [0 0 1], 'LineWidth', 2);
    end
    if (bSweepV)
        linePlume_sweep_v = line(x, count_sweep_v);
        set(linePlume_sweep_v, 'Color',  [0 1 0], 'LineWidth', 2);
    end
    if (bSweepH)
        linePlume_sweep_h = line(x, count_sweep_h);
        set(linePlume_sweep_h, 'Color',  [0 0.5 0.5], 'LineWidth', 2);
    end
    
    hold on 
    legStr_apf = sprintf('\\fontsize{9} APF-based sampling');
    legStr_rnd = '\fontsize{9} random sampling';
    legStr_sweep_h = '\fontsize{9} horizontal sweep sampling';
    legStr_sweep_v = '\fontsize{9} vertical sweep sampling';

    %%TODO
    hLegend = legend(legStr_apf, legStr_rnd, legStr_sweep_h, legStr_sweep_v, 'location', 'NorthEast' );
    set(hLegend, 'LineWidth', 1);
    hold on
    file = [resultPath '/' 'result-' experimentType '_' mapType '-' gdmConfig '-' spConfig_apf '-' 'plume'];
    saveas(h, [file '.fig'], 'fig');
    print(h, [file '.png'], '-dpng', '-r300');
    save([file '.mat'], 'avgPlume_apf' , 'avgPlume_rnd', '', ...
        'avgPlume_sweep_h', 'avgPlume_sweep_v', ...
        'mean_apf', 'mean_rnd', 'mean_sweep_h', 'mean_sweep_v', ...
        'std_apf', 'std_rnd', 'std_sweep_h', 'std_sweep_v');
    tmp = sprintf('%s\t%s\t%s\t%s\t%s\n', num2str(spConfig_apf), num2str(mean_apf), ...
        num2str(mean_rnd), num2str(mean_sweep_v), num2str(mean_sweep_h));
    fid = fopen('plume.txt', 'a+');
    fprintf(fid, tmp);
    fclose(fid);
    
end


function [ P, PVar ] = doCreateGroundTruth(experimentType, experimentLabel, gdmType, ...    
        inputFolder, resultFolder, bVisualizeData, bNormalizeData, bNormalizeTime )
% This function creates a GDM for the given experiment Type and lable. 
% it returns the Mean and Variance map. This will be used for KL distance
% study.

    % select experiment corresponding to the input experimentLabel
    [experimentType, fileName, startInd, endInd, sweepsInd, mapCenter, mapSize, ...
        sourceLocation, gasType, experimentSubDescr] = selectExperiment(experimentLabel);
       
    % setting the indices to cut samples when extracting input data.
    % indices. This is only for the simulation experiments.
    startTime = 100;
    if strcmp(experimentType, 'simulation_2d')
        endTime = 20;
    else
        endTime = 100;
    end
    % extract input data
    inputData = extractInputData(inputFolder, experimentType, ...
        experimentLabel, fileName, startTime, endTime, startInd, endInd, ...
        bNormalizeData, bNormalizeTime, bVisualizeData);

    % select GDM
    [kernelWidth, cellSize, timeScale] = selectMetaParameters(experimentLabel);
    %[kernelWidth, cellSize, timescale] = [0.25 1.25 0.00];

    %% set output path ====================================================
    gdmConfig = sprintf('c%.2f-s%.2f', cellSize, kernelWidth);
    outputPath = [resultFolder '/' 'GroundTruth-' gdmConfig];
    
    mkdir(outputPath);
    experimentDescription = ['KL-fulldata'];
    preOutputFile = [outputPath '/' experimentDescription '-' gdmConfig];    
        
    %% Model #P ===========================================================
    time = endTime;
    [ P, PVar, kGdmvMap_P ] = doBuildGDM(inputData, time, ...
        mapCenter, mapSize, experimentLabel);
     
    P = P ./sum(P);
    PVar = PVar ./sum(PVar);
    P = P ./repmat(sum(P,2),[1 size(P,2)]);
    PVar = PVar ./repmat(sum(PVar,2),[1 size(PVar,2)]);
    
    modelFile = [preOutputFile '-' 'FullModel' '.mat'];
    save(modelFile, 'kGdmvMap_P');
    
end


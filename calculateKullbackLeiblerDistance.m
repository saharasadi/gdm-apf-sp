function [Kullback_distance_mean Kullback_distance_var] = calculateKullbackLeiblerDistance(P, PVar, Q, QVar)

    if size(P,2)~=size(Q,2)
        error('the number of columns in P and Q should be the same');
    end

    if sum(~isfinite(P(:))) + sum(~isfinite(Q(:)))
        error('the inputs contain non-finite values!') 
    end

    % normalizing the P and Q
    %if size(Q,1)==1
    Q = Q ./sum(Q);
    QVar = QVar ./sum(QVar);
    %temp =  P.*log(P./repmat(Q,[size(P,1) 1]));
    temp =  0.5 * P.*log(P./repmat(Q,[size(P,1) 1])) + 0.5 * Q.*log(Q./repmat(P,[size(Q,1) 1]));
    temp(isnan(temp))=0;% resolving the case when P(i)==0
    dist = sum(temp,2);

    %tempVar = PVar.*log(PVar./repmat(QVar,[size(PVar,1) 1]));
    tempVar = 0.5 * PVar.*log(PVar./repmat(QVar,[size(PVar,1) 1])) + 0.5 * QVar.*log(QVar./repmat(PVar,[size(QVar,1) 1]));
    tempVar(isnan(tempVar))=0;% resolving the case when P(i)==0
    distVar = sum(tempVar,2);

    Kullback_distance_mean = dist;
    Kullback_distance_var = distVar;

end
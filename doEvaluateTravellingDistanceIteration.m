function [ ] = doEvaluateTravellingDistanceIteration( experimentType, mapType, samplingMethod, dataType, inSpConfig, ...
            gdmConfig, experimentDescription, totalNumRun, totalNumSamples, ...
            bShowVisualize)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

    bSweepV = true;
    bSweepH = true;
    bRand = true;
    bAPF = true;
    resultPath = ['result/' experimentType '/' samplingMethod];
    mkdir(resultPath);
    
    
    modelType = 'fulldata';
    spConfig_apf = inSpConfig; 
    spConfig_rnd = sprintf('M%.1f-V%.1f-R%.1f-C%.1f', 0, 0, 0, 0);
    
    avgDist_sweep_v = zeros(1, 3);
    avgDist_sweep_h = zeros(1, 3);
    avgDist_rnd = zeros(1, 3);
    avgDist_apf = zeros(1, 3);
    
    mean_sweep_v = zeros(1, 3);
    mean_sweep_h = zeros(1, 3);
    mean_rnd = zeros(1, 3);
    mean_apf = zeros(1, 3);
    
    std_sweep_v = zeros(1, 3);
    std_sweep_h = zeros(1, 3);
    std_rnd = zeros(1, 3);
    std_apf = zeros(1, 3);

    sumDist_rnd = zeros(totalNumRun, 3);
    sumDist_apf = zeros(totalNumRun, 3);
    sumDist_sweep_h = zeros(totalNumRun, 3);
    sumDist_sweep_v = zeros(totalNumRun, 3);

    if (bRand)
       dirNamePrefix = ['output/' experimentType '/' 'random'];
       for itr=1:1:totalNumRun
           dist1 = zeros(totalNumSamples,2);
           dist2 = zeros(totalNumSamples,1);
           pos = zeros(totalNumSamples+1, 2);

           dirName = [dirNamePrefix '/' 'SP-' num2str(itr) '-' spConfig_rnd];
           fileName = [dirName '/' 'KL' '-' experimentType '-' 'filaments100' '_' modelType '-' ...
               gdmConfig '-' spConfig_rnd '-' dataType '.mat'];
           data = load(fileName);       
           pos(:,1:2) = data.pos(:,1:2);           
           for k=1:1:totalNumSamples
               dist1(k,1) = abs(pos(k+1,1) - pos(k,1));
               dist1(k,2) = abs(pos(k+1,2) - pos(k,2));
               dist2(k,1) = sqrt(dist1(k,1).^2+dist1(k,2).^2);
           end           
           sumDist_rnd(itr,1:3) = [sum(dist1(:,1)) sum(dist1(:,2)) sum(dist2(:,1))];
       end
       mean_rnd(1, 1:3) = mean(sumDist_rnd);
       std_rnd(1, 1:3) = std(sumDist_rnd, 0);
       avgDist_rnd(1,1:3) = (1/totalNumRun) .* sum(sumDist_rnd(:,1:3));
    end

    if (bAPF)
       spConfig_apf = inSpConfig;
       dirNamePrefix = ['output/' experimentType '/' 'APF'];
       for itr=1:1:totalNumRun
           dist1 = zeros(totalNumSamples,2);
           dist2 = zeros(totalNumSamples,1);
           pos = zeros(totalNumSamples+1, 2);

           dirName = [dirNamePrefix '/' 'SP-' num2str(itr) '-' spConfig_apf];
           fileName = [dirName '/' 'KL' '-' experimentType '-' 'filaments100' '_' modelType '-' ...
               gdmConfig '-' spConfig_apf '-' dataType '.mat'];
           if (exist(fileName, 'file') == false)
               return
           end
           data = load(fileName);       
           pos(:,1:2) = data.pos(:,1:2);           
           for k=1:1:totalNumSamples
               dist1(k,1) = abs(pos(k+1,1) - pos(k,1));
               dist1(k,2) = abs(pos(k+1,2) - pos(k,2));
               dist2(k,1) = sqrt(dist1(k,1).^2+dist1(k,2).^2);
           end           
           sumDist_apf(itr,1:3) = [sum(dist1(:,1)) sum(dist1(:,2)) sum(dist2(:,1))];
       end
       mean_apf(1,1:3) = mean(sumDist_apf);
       std_apf(1,1:3) = std(sumDist_apf, 0);
       avgDist_apf(1,1:3) = (1/totalNumRun) .* sum(sumDist_apf(:,1:3));
    end

    if (bSweepH)
       dirNamePrefix = ['output/' experimentType '/' 'sweep-horizontal'];    
       for itr=1:1:totalNumRun
           dist1 = zeros(totalNumSamples,2);
           dist2 = zeros(totalNumSamples,1);
           pos = zeros(totalNumSamples+1, 2);

           dirName = [dirNamePrefix '/' 'SP-' num2str(itr) '-' spConfig_rnd];
           fileName = [dirName '/' 'KL' '-' experimentType '-' 'filaments100' '_' modelType '-' ...
               gdmConfig '-' spConfig_rnd '-' dataType '.mat'];
           data = load(fileName);       
           pos(:,1:2) = data.pos(:,1:2);           
           for k=1:1:totalNumSamples
               dist1(k,:) = abs(pos(k+1,:) - pos(k,:));
               dist2(k,1) = sqrt(dist1(k,1).^2+dist1(k,2).^2);
           end           
           sumDist_sweep_h(itr,1:3) = [sum(dist1(:,1)) sum(dist1(:,2)) sum(dist2(:,1))];
       end
       mean_sweep_h(1,1:3) = mean(sumDist_sweep_h);
       std_sweep_h(1,1:3) = std(sumDist_sweep_h, 0);
       avgDist_sweep_h(1, 1:3) = (1/totalNumRun) .* sum(sumDist_sweep_h(:,1:3));
    end

    if (bSweepV)
       dirNamePrefix = ['output/' experimentType '/' 'sweep-horizontal'];
       for itr=1:1:totalNumRun
           dist1 = zeros(totalNumSamples,2);
           dist2 = zeros(totalNumSamples,1);
           pos = zeros(totalNumSamples+1, 2);

           dirName = [dirNamePrefix '/' 'SP-' num2str(itr) '-' spConfig_rnd];
           fileName = [dirName '/' 'KL' '-' experimentType '-' 'filaments100' '_' modelType '-' ...
               gdmConfig '-' spConfig_rnd '-' dataType '.mat'];
           data = load(fileName);       
           pos(:,1:2) = data.pos(:,1:2);           
           for k=1:1:totalNumSamples
               dist1(k,:) = abs(pos(k+1,:) - pos(k,:));
               dist2(k,1) = sqrt(dist1(k,1).^2+dist1(k,2).^2);
           end           
           sumDist_sweep_v(itr,1:3) = [sum(dist1(:,1)) sum(dist1(:,2)) sum(dist2(:,1))];
       end
       mean_sweep_v(1,1:3) = mean(sumDist_sweep_v);
       std_sweep_v(1,1:3) = std(sumDist_sweep_v, 0);
       avgDist_sweep_v(1,1:3) = (1/totalNumRun) .* sum(sumDist_sweep_v(:,1:3));
    end

    maxY = 100;  minY = 0;    %!!!
    maxX = totalNumRun; minX = 1;    %!!!

    close all;
    h = figure('Units', 'pixels', 'Position', [100 100 1400 800]);
    if bShowVisualize
        set(h, 'visible', 'on');
    else
        set(h, 'visible', 'off');
    end
    
    txtTitle = ({['Total travelled distance in each run ' mapType ' maps'] ...
        ['created by different sampling strategies (' experimentType 'experiment)']}); 
    hTitle = title(txtTitle);

    hXLabel = xlabel('Run number');          
    hYLabel = ylabel('Total travelled distance');

    %axis([minX maxX minY maxY]);
    set(gca, 'LineWidth', 1);
    box on

    set( hTitle, 'FontSize', 14, 'FontWeight' , 'bold');
    set( hXLabel, 'FontSize', 12, 'FontWeight' , 'bold');
    set( hYLabel, 'FontSize', 12, 'FontWeight' , 'bold');

    x = [1:totalNumRun]';
    lineDist_apf = [];
    lineDist_rnd = [];
    lineDist_sweep_v = [];
    lineDist_sweep_h = [];
    
    if (bAPF)
        a = mean_apf(1,3) * ones(totalNumRun, 1);
        lineDist_apf = line(x, a);
        set(lineDist_apf, 'Color',  [1 0 0], 'LineWidth', 2);
    end
    if (bRand)
        a = mean_rnd(1,3) * ones(totalNumRun, 1);
        lineDist_rnd = line(x, a);
        set(lineDist_rnd, 'Color',  [0 0 1], 'LineWidth', 2);
    end
    if (bSweepV)
        a = mean_sweep_v(1,3) * ones(totalNumRun, 1);
        lineDist_sweep_v = line(x, a);
        set(lineDist_sweep_v, 'Color',  [0 1 0], 'LineWidth', 2);
    end
    if (bSweepH)
        a = mean_sweep_h(1,3) * ones(totalNumRun, 1);
        lineDist_sweep_h = line(x, a);
        set(lineDist_sweep_h, 'Color',  [0 0.5 0.5], 'LineWidth', 2);
    end
    
    hold on 
    legStr_apf = sprintf('\\fontsize{9} APF-based sampling');
    legStr_rnd = '\fontsize{9} random sampling';
    legStr_sweep_h = '\fontsize{9} horizontal sweep sampling';
    legStr_sweep_v = '\fontsize{9} vertical sweep sampling';
    tmp = sprintf('%s\t%s\t%s\t%s\t%s\n', num2str(spConfig_apf), num2str(mean_apf), ...
        num2str(mean_rnd), num2str(mean_sweep_v), num2str(mean_sweep_h));
    fid = fopen('travelling-distance.txt', 'a+');
    %%TODO
    hLegend = legend(legStr_apf, legStr_rnd, legStr_sweep_h, legStr_sweep_v, 'location', 'NorthEast' );
    set(hLegend, 'LineWidth', 1);
    hold on
    tmp = sprintf('%s\t%s\t%s\t%s\t%s\n', num2str(spConfig_apf), num2str(mean_apf), ...
        num2str(mean_rnd), num2str(mean_sweep_v), num2str(mean_sweep_h));
    fid = fopen('travelling-distance.txt', 'a+');
    fprintf(fid, tmp);
    fclose(fid);
    file = [resultPath '/' 'result-' experimentType '_' mapType '-' gdmConfig '-' spConfig_apf '-' 'distance'];
    saveas(h, [file '.fig'], 'fig');
    print(h, [file '.png'], '-dpng', '-r300');
    save([file '.mat'], 'avgDist_apf' , 'avgDist_rnd', '', ...
        'avgDist_sweep_h', 'avgDist_sweep_v', ...
        'mean_apf', 'mean_rnd', 'mean_sweep_h', 'mean_sweep_v', ...
        'std_apf', 'std_rnd', 'std_sweep_h', 'std_sweep_v');
end


%This code selects randoml N samples and runs GDM optimization to select
%cell size and kernel width. 
%% Setup ==================================================================
clc;
close all;
clear all;

totalSamplesSet = [1200:100:1200];
numTrials = 10;

% set input experiment
% experimentLabel = 'corridor', 'smallnet',  
%                   'no_obstacle_16x4', 'with_obstacle_up_16x4'] 
%                   'mox_sensor_model',  'with_obstacle_two_60x20'
% for 'with_obstacle_two_60x20' you need to add the path manually if you want to change datasset to 
% the ones with change in conditions such as gas source location, and release rate.
experimentLabel = 'mox_sensor_model';

gdmType = '2D';
% set optimization function
metric = 'NLPD'; % [NLPD, avErr] optimise for min NLPD

%% Select Experiment ======================================================
% select experiment corresponding to the input experimentLabel
[experimentType, fileName, startInd, endInd, sweepsInd, ...
    mapCenter, mapSize, ...
    sourceLocation, gasType, experimentSubDescr] = selectExperiment(experimentLabel);

% setting the indices to cut samples when extracting input data.
% indices. This is only for the simulation experiments.
startTime = 100;
if strcmp(experimentType, 'simulation_2d')
    endTime = 20;
else
    endTime = 100;
end

time = endTime;

%% select input data ======================================================
inputFolder = 'data';

% extract input data
bNormalizeData = false;
bNormalizeTime = false;
bVisualizeData = true;
bEpsilon = false;
bSaveLogs = true;

%% SELECT A RANDOM INITIAL POSITION =======================================

% flag to decide if we want to run optimization for constant cell size.
bConstantCellSize = false;
if stcmp(experimentType, 'simulation_ros')
    constantCellSize = 0.25;
else
    constantCellSize = 0.05;
end

% To run optimization on all meta-parameters
scale = 1;

% set the start time - this is used to create a folder in the result path
startTime = clock;
strStartTimeId = [sprintf('%i',startTime(1)) '_' ...
    sprintf('%02i',startTime(2)) '_' sprintf('%02i',startTime(3)) '-' ...
    sprintf('%02i',startTime(4)) '_' sprintf('%02i',startTime(5)) '_' ...
    sprintf('%02.0f',startTime(6))];
    
%Select numSamples randomly, run GDM optimization.
for k = 1:size(totalSamplesSet, 2)

    outputData = [];
    totalSamples = totalSamplesSet(1, k);
    for i=1:numTrials
        resultFolder = 'result_optimize_gdm';
        [optimizedResult, resultFilePath] = mainOptimize(experimentLabel, experimentType, ...
            inputFolder, resultFolder, strStartTimeId, ...
            bEpsilon, bNormalizeTime, bNormalizeData, bVisualizeData, bSaveLogs, ...
            gdmType, metric, ...
            startTime, endTime, ...
            bConstantCellSize, constantCellSize, ...
            scale, i, totalSamples);
        outputData = [outputData; optimizedResult];
        close all;
    end
    save([resultFilePath '/../' num2str(totalSamples) '.mat'], ...
        'outputData');
end



% X = [];
% bVerbose = false;
% pos = [];
% 
% %for now, the initial sensor id is selected randomly
% RandStream.setGlobalStream ...
%      (RandStream('mt19937ar','seed',sum(100*clock)));
% 
% rd = randi(size(inputData,1), [1 numSamples]); %received as input
% selectedIdx = rd;
% pos =  [pos; inputData(rd,3) inputData(rd,4)];
% simData = inputData(rd, :);
% 
% %timeTarget = time(2);
% simData(:,2) = (simData(:,2) - min(simData(:,2)))...
%     ./(max(simData(:,2))-min(simData(:,2)));
% 


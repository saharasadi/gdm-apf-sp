function [ ] = doEvaluatePerformance(experimentLabel, samplingMethod, selectionType, ...
    combination, totalNumRun, totalNumSamples, bShowVisualize, measure)
%UNTITLED9 Summary of this function goes here
%=========================================================================
% This is a program to evaluate sensor planning output using several
% evaluation measures and also plotting/map generation functions
% Sahar Asadi, 2012-02-01
%=========================================================================
%%

%clc
close all

%% To select one of the defined evaluation measures =======================
%'EMDist' --> Earth Movers Distance between two distributions
%'histogramDist' --> histogram based distance between two distributions
%'quadDist' --> Quadratic Distance between two distributions (this is
%               histrogrambased too
%'KL' --> Kullback-Leibler Distance between two ditributions
%'scaledMap' --> NOTE: this is not working but it meant to scales all generated maps
%'diffMap' --> Generates the difference map of two created distribution models
%'distance' --> to sum the total Euclidean distance between each two consecutive sampling points
%               if the experiment is performed several times, it averages
%               over all.
%'coverage' --> to compute the total divergance from the horizontal and vertical center lines
%               of the environment (distances are Euclidean). If the
%               experiment is performed serveral times, it averages over
%               all.
%'sourceEstimate' --> to estimate the source location from the created
%                     predictive mean and variance gas distribution maps (estimation based on
%                     different criteria are computed).
%'plume' --> NOTE: this is not implemented but it meant to compute ratio of the samples that
%            are sampled inside the plume.

%% selectExperiment();=================================================
    [experimentType, fileName, initInd, endInd, sweepsInd, ...
        mapCenter, mapSize, ...
        srcLocation, gasType, experimentSubDescr] = selectExperiment(experimentLabel);
    
    experimentDescription = ['KL-' experimentType '-filaments100_fulldata'];
        
%% SELECT GDM META PARAMETERS =========================================
% select GDM
    [kernelWidth, cellSize, timeScale] = selectMetaParameters(experimentLabel);
    
%% SET INPUT PATH
    gdmConfig = sprintf('c%.2f-s%.2f', cellSize, kernelWidth);
    
    spConfig = sprintf('M%.1f-V%.1f-R%.1f-C%.1f', ...
        combination(1,1), combination(1,2), combination(1,3), combination(1,4)); 
    
%% PERFORM EVALUATION    
switch measure
%% KL creates plots of KL results already calculated in running experiments.
    case  'KL'
        bRand = true; %to select to use random sampling KL values in the splots or not.
        bAPF = true; %to select to use APF-based sampling KL values in the splots or not.
        mapType = 'mean'; %select between mean and variance
        dataType = 'KLValues'; %'FULLDATA'; 'Models'; 'Positions'; 'SAMPLES'; 'FullModel'
        doEvaluateKL(experimentType, mapType, samplingMethod, dataType, spConfig, ...
            gdmConfig, experimentDescription, ...
            totalNumRun, totalNumSamples, ...
            bShowVisualize);
        
%% source: distance to source  
    case 'source'
        mapType = 'mean'; %select between mean and variance
        dataType = 'Positions'; %'FULLDATA'; 'Models'; 'Positions'; 'SAMPLES'; 'FullModel'
        source = [2 2];
        doEvaluateDistanceToSource(experimentType, mapType, samplingMethod, dataType, spConfig, ...
            gdmConfig, source, totalNumRun, totalNumSamples, ...
            bShowVisualize)    
        
%% distance: averages the total distance travelled by the mobile sensor on all runs of an experiment    
    case 'distance'
        samplingMethod = 'APF'; %'random'
        dataType = 'Positions'; %'FULLDATA'; 'Models'; 'Positions'; 'SAMPLES'; 'FullModel'
        bRand = true; %to select to use random sampling KL values in the splots or not.
        bAPF = true; %to select to use APF-based sampling KL values in the splots or not.
        mapType = 'mean'; %select between mean and variance

        %doCompDistance(experimentType, samplingMethod, dataType, totalNumRun, totalRunSamples);
        doEvaluateTravellingDistanceIteration( experimentType, mapType, samplingMethod, dataType, spConfig, ...
            gdmConfig, experimentDescription, totalNumRun, totalNumSamples, ...
            bShowVisualize);
        
%% coverage: averages on the vertical and horizontal distances travelled by the mobile sensor        
    case 'coverage'
        numX = mapSize(1,1)/cellSize;
        numY = mapSize(1,2)/cellSize;
        mapType = 'mean'; %select between mean and variance
        dataType = 'Positions'; %'FULLDATA'; 'Models'; 'Positions'; 'SAMPLES'; 'FullModel'
        doEvaluateCoverageIteration(experimentType, mapType, samplingMethod, dataType, spConfig, ...
            gdmConfig, experimentDescription, ...
            totalNumRun, totalNumSamples, numX, numY, ...
            bShowVisualize);
    case 'plume'
        samplingMethod = 'APF'; %'random'
        mapType = 'mean'; %select between mean and variance
        dataType = 'SAMPLES'; %'FULLDATA'; 'Models'; 'Positions'; 'SAMPLES'; 'FullModel'
        doEvaluatePlumeCoverageIteration(experimentType, mapType, samplingMethod, dataType, spConfig, ...
            gdmConfig, experimentDescription, totalNumRun, totalNumSamples, ...
            bShowVisualize);

%% NOT INTEGRATED
%     case 'scaledMap'
%         mapType = 'mean';
%         dataType = 'Models';
%         samplingMethod  = 'APF';
%         doGenerateMaps(experimentType, mapType, samplingMethod, dataType, ...
%             totalNumRun, totalNumSamples, ...
%             bShowVisualize);
%  
% %% diffMap creates the difference map between two created models
%     case 'diffMap'
%         mapType = 'mean';
%         dataType = 'Models';
%         samplingMethod_1  = 'APF';
%         samplingMethod_2 = samplingMethod_1;
%         runIdx_1 = 1; %which run of the experiment #1
%         runIdx_2 = runIdx_1; %which run of the experiment #2        
%         iterationIdx_1 = 70; %which sampling iteratoin of experiment #1
%         iterationIdx_2 = 70; %which sampling iteratoin of experiment #2        
%         coeff_1 = combination; %which combination as experiment #1
%         coeff_2 = coeff_1; %which combination as experiment #2
%         doDifferMaps(experimentType, mapType, samplingMethod_1, samplingMethod_2, dataType, ...
%             runIdx_1, runIdx_2, iterationIdx_1, iterationIdx_2, ...
%             coeff_1, coeff_2, ...
%             bShowVisualize);           
        
%% NOT INTEGRATED.        
%     case 'sourceEstimate'
%         coeff = selectSensorPlanning();
%         runNum = 20;
%         iterationNum = 300;
%         samplingMethod = 'APF'; %'random'
%         dataType = 'Models'; %'FULLDATA'; 'Models'; 'Positions'; 'SAMPLES'; 'FullModel'
%         for i=1:1:size(coeff, 1)
%             doSourceEstimate( experimentType, samplingMethod, dataType, ...
%                 runNum, iterationNum, coeff(i, :), ...
%                 bShowVisualize);
%         end
%     case 'histogramDist'
%         runNum = 20;
%         iterationNum = 300;
%         coeff = selectSensorPlanning(); %[0.1 0.4 0.1 0.4];
%         mapType = 'mean';
%         samplingMethod = 'APF'; %'random'
%         dataType = 'Models'; %'FULLDATA'; 'Models'; 'Positions'; 'SAMPLES'; 'FullModel'
%         for i=1:1:size(coeff, 1)
%             doHistogramIntersect( experimentType, mapType, samplingMethod, ...
%                 totalNumRun, yoyslRunSamples, dataType, combination);
%         end
%             
%     case 'quadDist'
%         coeff = [0.1 0.4 0.1 0.4];
%         mapType = 'mean';
%         samplingMethod = 'APF'; %'random'
%         dataType = 'Models'; %'FULLDATA'; 'Models'; 'Positions'; 'SAMPLES'; 'FullModel'
%         doQuadDist( experimentType, mapType, samplingMethod, dataType, runNum, iterationNum, coeff );
%     
%     case 'EMDist'
%         runNum = 20;
%         iterationNum = 300;
%         coeff = selectSensorPlanning(); %[0.1 0.4 0.1 0.4];
%         mapType = 'mean';
%         samplingMethod = 'APF'; %'random'
%         dataType = 'Models'; %'FULLDATA'; 'Models'; 'Positions'; 'SAMPLES'; 'FullModel'
%         for i=1:1:size(coeff, 1)
%             doEMDist( experimentType, mapType, samplingMethod, dataType, runNum, iterationNum, coeff );
%         end
    otherwise
        disp('the selected measure is out of the scope');

end


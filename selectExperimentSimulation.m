function [experimentType, experimentLabel, experimentSubDescr, logFileType, ...
    mapCenter, mapSize, ...
    srcLocation, ...
    initInd, endInd, sweepsInd, ...
    calibrationFile, ...
    time, margin] = selectExperimentSimulation()

    experimentSubDescr = '';
    initInd = [];
    endInd = [];
    sweepsInd = [];
    srcLocation = [];
    calibrationFile = '';

    % SIMULATION
    %--------------------
    experimentSubDescr = 'For Sensor Planning';
    %experimentType = 'laminar';
    experimentType = 'obstacle';
    sweepsInd = [];
    calibrationFile = '';
    %experimentLabel = 'output50_filaments100_laminar_x2-y2';
    experimentLabel = 'output50_filaments100_obstacle';
    logFileType = 'Simulation';
    mapCenter = [ 8.0  2.0]; 
    mapSize = [16  4]; 
    srcLocation = [2.0 2.0];
    initInd = []; endInd = []; % full experiment
    margin = [0 0.3 0 0]; %[0 0.1 0 0];
    time = [50 50 50]; %[start end target]
    %--------------------

    % Outdoor Experiments
    %---------------------
    % % 2007_09_07a-ORU_Outdoor
    % experimentLabel = '2007_09_07a-ORU_Outdoor-All';
    % logFileType = 'LegacyTxt';
    % mapCenter = [ -4.0   3.0 ]; mapSize   = [ 18.0  18.0 ];
    % sweepsInd = [285,2950, 2951,5419];

    % 3-Room Experiments
    %--------------------
    % % 2007_09_06c-ORU_MicroscopeRoom
    % experimentLabel = '2007_09_06c-ORU_MicroscopeRoom-All';
    % logFileType = 'LegacyTxt';
    % mapCenter = [ -6.5  -5.0 ]; mapSize   = [ 26.0  18.0 ];
    % initInd = 241; endInd = 5753;
    % sweepsInd = [288,3054, 3055,5753];
    % calibrationFile = '';
    % comments:
    % - only a few real peaks
    % - baseline correction shouldn't be used because of a low frequent rise, 
    %   which is due to the higher concentration in the central room
    % - TGS2611 (4/26) is very noisy but still shows very pronounced peaks
    % - TGS2602 (5/32) saturates several times but also shows more peaks
    % - TGS2620 (3/20), TGS2600_2 (2/14) and TGS2600_2 (6/38) seem to be OK
    % - as always, TGS4161 (1/14) shows only noise

    % 3D-Experiments, Corridor
    %--------------------------
    % 2008_10_14a-Corridor_Sweep
    % experimentLabel = '2008_10_14a-Corridor_Sweep';
    % logFileType = 'PlayerLog3D';
    % mapCenter = [ 9.0  3.5 ]; mapSize   = [ 20.0  14.0 ];
    % initInd = 1;
    % endInd = 6249;
    % sweepsInd = [1,1594, 1595,3156, 3157,4689, 4690,6249];

    % 2008_10_14b-Corridor_Random
    % experimentLabel = '2008_10_14b-Corridor_Random';
    % logFileType = 'PlayerLog3D';
    % mapCenter = [ 9.0  3.5 ]; mapSize   = [ 20.0  14.0 ];
    % initInd = 1410;
    % endInd = 6130;
    % sweepsInd = [];

    % 2008_10_15a-Corridor_Random
    % experimentLabel = '2008_10_14b-Corridor_Random';
    % logFileType = 'PlayerLog3D';
    % mapCenter = [ 9.0  3.5 ]; mapSize   = [ 20.0  14.0 ];
    % initInd = 1;
    % endInd = 9790;
    % sweepsInd = [];

    % % 2008_10_15c-Corridor_Random
    % experimentLabel = '2008_10_15c-Corridor_Random';
    % logFileType = 'PlayerLog3D';
    % mapCenter = [ 8.0  3.5 ]; mapSize   = [ 20.0  14.0 ];

    % 2008_10_16a-Corridor_Random
    %-----------------------------
    % % % 2008_10_16a-Corridor_Random
    % % experimentLabel = '2008_10_16a-Corridor_Random';
    % % logFileType = 'PlayerLog3D';
    % % 2008_10_16a-Corridor_Random_bc_ww_0050_csf_00350
    % experimentLabel = '2008_10_16a-Corridor_Random_bc_ww_0050_csf_00350';
    % logFileType = 'PlayerLog3D';
    % mapCenter = [ 8.0  3.5 ]; mapSize   = [ 20.0  14.0 ];
    % srcLocation = [ 9.0  3.0  0.0 ];
    % % full experiment
    % initInd = 128; endInd = 7261;
    % % % first half (clearly different situation compared to the second half)
    % % experimentSubDescr = '1st_half';
    % % initInd = 128;
    % % endInd = 3550; %endInd = 2550;
    % % % second half (clearly different situation compared to the first half)
    % % experimentSubDescr = '2nd_half';
    % % initInd = 3551; endInd = 7261;

    %-----------------------------
    % 2008_10_16b-Corridor_Random
    %-----------------------------
    % % 2008_10_16b-Corridor_Random
    % experimentLabel = '2008_10_16b-Corridor_Random';
    % % % 2008_10_16b-Corridor_Random
    % % experimentLabel = '2008_10_16b-Corridor_Random_bc_ww_0050_csf_00350';
    % logFileType = 'PlayerLog3D';
    % mapCenter = [ 8.0  3.5 ]; mapSize   = [ 20.0  14.0 ];
    % srcLocation = [9.0 3.0 1.6];
    % initInd = 50; endInd = 7468;
    % % first half
    % % experimentSubDescr = 'part1';
    % % initInd = 1;
    % % %endInd = 3734;
    % % endInd = 2400;
    % % experimentSubDescr = 'part2';
    % % initInd = 3735;
    % % endInd = 6400;
    % % experimentSubDescr = 'part3';
    % % initInd = 3735;
    % % endInd = 6468;

    %-----------------------------
    % % % 2008_10_16c-Corridor_Sweep
    % % experimentLabel = '2008_10_16c-Corridor_Sweep';
    % % 2008_10_16c-Corridor_Sweep (after adaptive baseline correction)
    % experimentLabel = '2008_10_16c-Corridor_Sweep_bc_ww_0050_csf_00350';
    % logFileType = 'PlayerLog3D';
    % mapCenter = [ 8.0  3.5 ]; mapSize = [ 22.0  14.0 ];
    % % mapCenter = [ 8.0  3.5 ]; mapSize = [ 14.0  7.0 ]; % to create raw data movie
    % % mapCenter = [ 0.0  0.0 ]; mapSize = [ 64.0  48.0 ]; % to overlay with the spatial map (use cell size = 10cm)
    % initInd = 32; endInd = 5290; % full experiment

    %-----------------------------
    % % % 2008_10_16d-Corridor_Sweep
    % % experimentLabel = '2008_10_16d-Corridor_Sweep';
    % % 2008_10_16d-Corridor_Sweep (after adaptive baseline correction)
    % experimentLabel = '2008_10_16d-Corridor_Sweep_bc_ww_0050_csf_00350';
    % logFileType = 'PlayerLog3D';
    % mapCenter = [ 8.0  3.5 ]; mapSize = [ 22.0  14.0 ];
    % % mapCenter = [ 8.0  3.5 ]; mapSize = [ 14.0  7.0 ]; % to create raw data movie
    % % mapCenter = [ 0.0  0.0 ]; mapSize = [ 64.0  48.0 ]; % to overlay with the spatial map (use cell size = 10cm)
    % initInd = 1; endInd = 6344; % full experiment

    % %%#########################################################################
    % %%#########################################################################
    % % % 2008_10_22a-Corridor_Random
    % % experimentLabel = '2008_10_22a-Corridor_Random';
    % % 2008_10_22a-Corridor_Random (after adaptive baseline correction)
    % experimentLabel = '2008_10_22a-Corridor_Random_bc_ww_0050_csf_00350';
    % logFileType = 'PlayerLog3D';
    % mapCenter = [ 8.0  3.5 ]; mapSize = [ 22.0  14.0 ];
    % % mapCenter = [ 8.0  3.5 ]; mapSize = [ 14.0  7.0 ]; % to create raw data movie
    % % mapCenter = [ 0.0  0.0 ]; mapSize = [ 64.0  48.0 ]; % to overlay with the spatial map (use cell size = 10cm)
    % srcLocation = [9.0 3.0 1.6];
    % initInd = 128; endInd = 9288; % 9100 samples (1/4: 2403; 1/3: 3161; 1/2: 4178; 2/3: 6195; 3/4: 6953)
    % sweepsInd = [128 3161 3162 6195 6196 9288]; % just a guess
    % %%#########################################################################
    % %%#########################################################################

    %%#########################################################################
    %%#########################################################################
    % % 2008_10_23a-Corridor_Random
    % experimentLabel = '2008_10_23a-Corridor_Random';
    %---
    % % 2008_10_23a-Corridor_Random (after adaptive baseline correction)
    %experimentLabel = '2008_10_23a-Corridor_Random_bc_ww_0050_csf_00350';
    % experimentLabel = '2008_10_23a-Corridor_Random_bc_ww_0050_csf_00700_TGS2620C_opt';
    %logFileType = 'PlayerLog3D';
    %mapCenter = [ 8.0  3.5 ]; mapSize = [ 22.0  14.0 ];
    % mapCenter = [ 8.0  3.5 ]; mapSize = [ 14.0  7.0 ]; % to create raw data movie
    % mapCenter = [ 0.0  0.0 ]; mapSize = [ 64.0  48.0 ]; % to overlay with the spatial map (use cell size = 10cm)
    %srcLocation = [6.1 3.2 1.6];
    %initInd = 128; endInd = 8546; % full experiment
    % initInd = 128; endInd = round((8546 - 128) / 3) + 128; % first third
    % initInd = round((8546 - 128) / 3) + 128; endInd = round((8546 - 128) * 2 / 3) + 128; % second third
    % initInd = round((8546 - 128) * 2 / 3) + 128; endInd = 8546; % third third
    % initInd = round((8546 - 128) / 3) + 128; endInd = 8546; % second and third third
    %---
    % experimentLabel = '2008_10_23a_bc_TGS2620-used';
    % logFileType = 'FreiburgGpmm';
    % mapCenter = [ 8.0  3.5 ]; mapSize = [ 22.0  14.0 ];
    % srcLocation = [6.1 3.2 1.6];
    %---
    % experimentLabel = '2008_10_23a_temperature_Anemo-used';
    % logFileType = 'FreiburgGpmm';
    % mapCenter = [ 8.0  3.5 ]; mapSize = [ 22.0  14.0 ];
    % srcLocation = [6.1 3.2 1.6];
    %%#########################################################################
    %%#########################################################################

    % Template Experiment
    %---------------------
    % experimentLabel = '???';
    % mapCenter = [ -6.5  -5.0 ]; mapSize   = [ 26.0  18.0 ];
    % sweepsInd = [];
    % stopsInd = [];

    %experimentLabel = '2007_09_11-ORU_Outdoor-All';

    %experimentLabel = '2007_09_07a-ORU_Outdoor-Driving_Only';
    %experimentLabel = '2007_09_07a-ORU_Outdoor-Meas_Only';
    %experimentLabel = '2007_09_06b-ORU_Outdoor-All';
    %experimentLabel = '2007_09_06b-ORU_Outdoor-Meas_Only';
    %experimentLabel = '2007_09_06b-ORU_Outdoor-Driving_Only';
    %experimentLabel = '2007_09_06a-ORU_Outdoor-All';
    %experimentLabel = '2007_09_06a-ORU_Outdoor-Driving_Only';
    %experimentLabel = '2007_09_06a-ORU_Outdoor-Meas_Only';

    %experimentLabel = '2007_09_06c-ORU_MicroscopeRoom-Meas_Only';
    %experimentLabel = '2007_09_06c-ORU_MicroscopeRoom-Driving_Only';
    %experimentLabel = '2007_09_06c-ORU_MicroscopeRoom-All';


    %experimentLabel = '2007_09_07b-ORU_MicroscopeRoom-Driving_Only';
    %experimentLabel = '2007_09_07b-ORU_MicroscopeRoom-Meas_Only';
    %experimentLabel = '2007_09_07b-ORU_MicroscopeRoom-All';
    %experimentLabel = '2007_09_07c-ORU_MicroscopeRoom-Driving_Only';
    %experimentLabel = '2007_09_07c-ORU_MicroscopeRoom-Meas_Only';
    %experimentLabel = '2007_09_07c-ORU_MicroscopeRoom-All';
    %experimentLabel = '2007_09_08-ORU_Corridor-Driving_Only';
    %experimentLabel = '2007_09_08-ORU_Corridor-Meas_Only';
    %experimentLabel = '2007_09_08-ORU_Corridor-All';
    %experimentLabel = '2007_09_09a-ORU_Corridor-Driving_Only';
    %experimentLabel = '2007_09_09a-ORU_Corridor-Meas_Only';
    %experimentLabel = '2007_09_09a-ORU_Corridor-All';
    %experimentLabel = '2007_09_09b-ORU_Corridor-Driving_Only';
    %experimentLabel = '2007_09_09b-ORU_Corridor-Meas_Only';
    %experimentLabel = '2007_09_09b-ORU_Corridor-All';
    %experimentLabel = '2007_09_09c-ORU_Corridor-Driving_Only';
    %experimentLabel = '2007_09_09c-ORU_Corridor-Meas_Only';
    %experimentLabel = '2007_09_09c-ORU_Corridor-All';
    %experimentLabel = '2007_09_10-ORU_MicroscopeRoom-All';
    %experimentLabel = '2007_09_02a-ORU_Corridor-All';

    % parameters that specify the mapping area
    % % Corridor
    % mapCenter = [  9.0  3.3 ]; mapSize   = [ 20.0  8.0 ];
    % % 3-Rooms (Microscope Room)
    % mapCenter = [ -6.5  -5.0 ]; mapSize   = [ 24.0  18.0 ];
    % % Outdoor
    % mapCenter = [ -3.5   3.0 ]; mapSize   = [ 16.0  16.0 ];
    % % Outdoor 2
    % mapCenter = [ -4.0   3.0 ]; mapSize   = [ 18.0  18.0 ];
end
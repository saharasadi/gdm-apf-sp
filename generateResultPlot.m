comb = [0.0 0.1 0.0 0.9];

spConfig_apf = sprintf('M%.1f-V%.1f-R%.1f-C%.1f', comb(1,1), comb(1,2), comb(1,3), comb(1,4));
sp_apf = sprintf('(%.1f, %.1f, %.1f)', comb(1,1), comb(1,2), comb(1,4));

expType = 'laminar-x5-y3-';
load([expType spConfig_apf '.mat']);

h = figure
axes('FontSize',12);

hold on
kl_1 = line([1:1:300]', muM_apf);
set(kl_1, 'Color',  [0 0 1], 'LineWidth', 2);

hold on
kl_2 = line([1:1:300]', muM_rnd);
set(kl_2, 'Color',  [1 0 0], 'LineWidth', 2);

hold on
kl_3 = line([1:1:300]', muM_sweep_h);
set(kl_3, 'Color',  [0.3 0.3 0.4], 'LineWidth', 2);

hold on
kl_4 = line([1:1:300]', muM_sweep_v);
set(kl_4, 'Color',  [0.1 0.8 0.1], 'LineWidth', 2);

str_1 = ['APFSP ' sp_apf];
str_2 = 'Random Sampling';
str_3 = 'Sweep Horizontal';
str_4 = 'Sweep Vertical';

hold on
hlegend = legend([kl_1 kl_2 kl_3 kl_4], str_1, str_2, str_3, str_4);
set(hlegend,'FontWeight','bold','FontSize',14);

box on;
grid on;

xlabel('Sampling iteration','FontWeight','bold','FontSize',14);
ylabel('KL-distance', 'FontWeight','bold','FontSize',14);

function [ ] = doEvaluateCoverageIteration( experimentType, mapType, samplingMethod, dataType, inSpConfig, ...
            gdmConfig, experimentDescription, ...
            totalNumRun, totalNumSamples, numX, numY, ...
            bShowVisualize)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

    bSweepV = true;
    bSweepH = true;
    bRand = true;
    bAPF = true;
    [experimentType, experimentLabel, experimentSubDescr, logFileType, ...
        mapCenter, mapSize, srcLocation, initInd, endInd, sweepsInd, ...
        calibrationFile, time, margin] = selectExperimentSimulation();
    
    [cellSize kernelWidth] = selectGDM();
    mapCellSize = [ cellSize cellSize ]; 
    
    [ numOfMapCells mapCenter mapCellSize mapLowerLeft ] = ...
            getMapGeometry(mapCenter, mapSize, mapCellSize);
    mapGeometry = [ numOfMapCells mapCenter mapCellSize mapLowerLeft ];
    mapCellsX = mapGeometry(1,1); % here it is assumed that mapCellsX is an integer, otherwise use "sign(mapGeometry(1,1))*ceil(abs(mapGeometry(1,1)));"
    mapCellsY = mapGeometry(1,2); % here it is assumed that mapCellsY is an integer, otherwise use "sign(mapGeometry(1,2))*ceil(abs(mapGeometry(1,2)));"
    area = zeros(mapCellsX,mapCellsY,2);

    mapCellSizeX = mapGeometry(1,5); 
    mapCellSizeY = mapGeometry(1,6);
    mapLowerLeftX = mapGeometry(1,7);
    mapLowerLeftY = mapGeometry(1,8);
    [area(:,:,1), area(:,:,2)] = ndgrid(...
        mapLowerLeftX+0.5*mapCellSizeX:mapCellSizeX:mapLowerLeftX+mapCellsX*mapCellSizeX, ...
        mapLowerLeftY+0.5*mapCellSizeY:mapCellSizeY:mapLowerLeftY+mapCellsY*mapCellSizeY);

    resultPath = ['result/' experimentType '/' samplingMethod];
    mkdir(resultPath);
    
    if (strcmp(samplingMethod, 'APF'))
        coeff = selectSensorPlanning();
        combination = coeff(coeff(:,3) == 0, :);
    end
    
    totalNumComb = size(combination, 1);
    
    modelType = 'fulldata';
    spConfig_apf = inSpConfig; 
    spConfig_rnd = sprintf('M%.1f-V%.1f-R%.1f-C%.1f', 0, 0, 0, 0);
    
    avgCoverage_sweep_v = zeros(1, 1);
    avgCoverage_sweep_h = zeros(1, 1);
    avgCoverage_rnd = zeros(1, 1);
    avgCoverage_apf = zeros(1, 1);
    
    mean_sweep_v = zeros(1, 1);
    mean_sweep_h = zeros(1, 1);
    mean_rnd = zeros(1, 1);
    mean_apf = zeros(1, 1);
    
    std_sweep_v = zeros(1, 1);
    std_sweep_h = zeros(1, 1);
    std_rnd = zeros(1, 1);
    std_apf = zeros(1, 1);

    count_apf = zeros(totalNumRun, 1);
    count_rnd = zeros(totalNumRun, 1);
    count_sweep_h = zeros(totalNumRun, 1);
    count_sweep_v = zeros(totalNumRun, 1);
    
    if (bRand)
       dirNamePrefix = ['output/' experimentType '/' 'random'];
       for itr=1:1:totalNumRun
           score = zeros(numX, numY, 1);
           clear data
           clear measurement
           dirName = [dirNamePrefix '/' 'SP-' num2str(itr) '-' spConfig_rnd];
           fileName = [dirName '/' 'KL' '-' experimentType '-' 'filaments100' '_' modelType '-' ...
               gdmConfig '-' spConfig_rnd '-' dataType '.mat'];
           data = load(fileName);       
           pos = data.pos(:,1:2);   
           for k=1:1:totalNumSamples
              coveredCells = find(sqrt((area(:,:,1) - pos(k,1)).^2+ ...
                  (area(:,:,2) - pos(k,2)).^2) < 0.1);           
              score(coveredCells) = score(coveredCells) + 1; 

           end
           count_rnd(itr, 1) = size(find(score > 0), 1);
       end
       mean_rnd(1, 1) = mean(count_rnd, 1);
       std_rnd(1, 1) = std(count_rnd, 0);
       avgCoverage_rnd(1,1) = (1/totalNumRun) .* sum(count_rnd(:,1));
    end

    if (bAPF)
       dirNamePrefix = ['output/' experimentType '/' 'APF'];
       for itr=1:1:totalNumRun
           score = zeros(numX, numY, 1);
           clear data
           clear measurement
           dirName = [dirNamePrefix '/' 'SP-' num2str(itr) '-' spConfig_apf];
           fileName = [dirName '/' 'KL' '-' experimentType '-' 'filaments100' '_' modelType '-' ...
               gdmConfig '-' spConfig_apf '-' dataType '.mat'];
           if (exist(fileName, 'file') == false)
               return
           end
           data = load(fileName);       
           pos = data.pos(:,1:2);   
           for k=1:1:totalNumSamples
              coveredCells = find(sqrt((area(:,:,1) - pos(k,1)).^2+ ...
                  (area(:,:,2) - pos(k,2)).^2) < 0.1);           
              score(coveredCells) = score(coveredCells) + 1; 

           end
           count_apf(itr, 1) = size(find(score > 0), 1);
       end
       mean_apf(1,1) = mean(count_apf);
       std_apf(1,1) = std(count_apf, 0);
       avgCoverage_apf(1,1) = (1/totalNumRun) .* sum(count_apf(:,1));
    end

    if (bSweepH)
       dirNamePrefix = ['output/' experimentType '/' 'sweep-horizontal'];    
       for itr=1:1:totalNumRun
           score = zeros(numX, numY, 1);
           clear data
           clear measurement
           dirName = [dirNamePrefix '/' 'SP-' num2str(itr) '-' spConfig_rnd];
           fileName = [dirName '/' 'KL' '-' experimentType '-' 'filaments100' '_' modelType '-' ...
               gdmConfig '-' spConfig_rnd '-' dataType '.mat'];
           data = load(fileName);       
           pos = data.pos(:,1:2);   
           for k=1:1:totalNumSamples
              coveredCells = find(sqrt((area(:,:,1) - pos(k,1)).^2+ ...
                  (area(:,:,2) - pos(k,2)).^2) < 0.1);           
              score(coveredCells) = score(coveredCells) + 1; 
           end
           count_sweep_h(itr, 1) = size(find(score > 0), 1);
       end
       mean_sweep_h(1,1) = mean(count_sweep_h);
       std_sweep_h(1,1) = std(count_sweep_h, 0);
       avgCoverage_sweep_h(1, 1) = (1/totalNumRun) .* sum(count_sweep_h(:,1));
    end

    if (bSweepV)
       dirNamePrefix = ['output/' experimentType '/' 'sweep-vertical'];
       for itr=1:1:totalNumRun
           score = zeros(numX, numY, 1);
           clear data
           clear measurement
           dirName = [dirNamePrefix '/' 'SP-' num2str(itr) '-' spConfig_rnd];
           fileName = [dirName '/' 'KL' '-' experimentType '-' 'filaments100' '_' modelType '-' ...
               gdmConfig '-' spConfig_rnd '-' dataType '.mat'];
           data = load(fileName);       
           pos = data.pos(:,1:2);   
           for k=1:1:totalNumSamples
              coveredCells = find(sqrt((area(:,:,1) - pos(k,1)).^2+ ...
                  (area(:,:,2) - pos(k,2)).^2) < 0.1);           
              score(coveredCells) = score(coveredCells) + 1; 

           end
           count_sweep_v(itr, 1) = size(find(score > 0), 1);
       end
       mean_sweep_v(1,1) = mean(count_sweep_v);
       std_sweep_v(1,1) = std(count_sweep_v, 0);
       avgCoverage_sweep_v(1,1) = (1/totalNumRun) .* sum(count_sweep_v(:,1));
    end


    close all;
    h = figure('Units', 'pixels', 'Position', [100 100 1400 800]);
    if bShowVisualize
        set(h, 'visible', 'on');
    else
        set(h, 'visible', 'off');
    end
    
    txtTitle = ({['Plume Coverage in each run ' mapType ' maps'] ...
        ['created by different sampling strategies (' experimentType 'experiment)']}); 
    hTitle = title(txtTitle);

    hXLabel = xlabel('Run number');          
    hYLabel = ylabel('Number of Samples with gas measrument > 0.01');

    set(gca, 'LineWidth', 1);
    box on

    set( hTitle, 'FontSize', 14, 'FontWeight' , 'bold');
    set( hXLabel, 'FontSize', 12, 'FontWeight' , 'bold');
    set( hYLabel, 'FontSize', 12, 'FontWeight' , 'bold');

    x = [1:totalNumRun]';
    linePlume_apf = [];
    linePlume_rnd = [];
    linePlume_sweep_v = [];
    linePlume_sweep_h = [];
    hold on
    
    if (bAPF)
        linePlume_apf = line(x, count_apf);
        set(linePlume_apf, 'Color',  [1 0 0], 'LineWidth', 2);
    end
    if (bRand)
        linePlume_rnd = line(x, count_rnd);
        set(linePlume_rnd, 'Color',  [0 0 1], 'LineWidth', 2);
    end
    if (bSweepV)
        linePlume_sweep_v = line(x, count_sweep_v);
        set(linePlume_sweep_v, 'Color',  [0 1 0], 'LineWidth', 2);
    end
    if (bSweepH)
        linePlume_sweep_h = line(x, count_sweep_h);
        set(linePlume_sweep_h, 'Color',  [0 0.5 0.5], 'LineWidth', 2);
    end
    
    hold on 
    legStr_apf = sprintf('\\fontsize{9} APF-based sampling');
    legStr_rnd = '\fontsize{9} random sampling';
    legStr_sweep_h = '\fontsize{9} horizontal sweep sampling';
    legStr_sweep_v = '\fontsize{9} vertical sweep sampling';

    tmp = sprintf('%s\t%s\t%s\t%s\t%s\n', num2str(spConfig_apf), num2str(mean_apf), ...
        num2str(mean_rnd), num2str(mean_sweep_v), num2str(mean_sweep_h));
    fid = fopen('coverage.txt', 'a+');
    fprintf(fid, tmp);
    fclose(fid);
    %%TODO
    hLegend = legend(legStr_apf, legStr_rnd, legStr_sweep_h, legStr_sweep_v, 'location', 'NorthEast' );
    set(hLegend, 'LineWidth', 1);
    hold on
    file = [resultPath '/' 'result-' experimentType '_' mapType '-' gdmConfig '-' spConfig_apf '-' 'coverage'];
    saveas(h, [file '.fig'], 'fig');
    print(h, [file '.png'], '-dpng', '-r300');
    save([file '.mat'], 'avgCoverage_apf' , 'avgCoverage_rnd', '', ...
        'avgCoverage_sweep_h', 'avgCoverage_sweep_v', ...
        'mean_apf', 'mean_rnd', 'mean_sweep_h', 'mean_sweep_v', ...
        'std_apf', 'std_rnd', 'std_sweep_h', 'std_sweep_v');
    
end


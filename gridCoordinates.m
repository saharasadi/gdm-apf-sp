function [cellX, cellY] = gridCoordinates(x,y,mapGeometry,bOutsideIsInf)

mapCellSizeX = mapGeometry(1,5); 
mapCellSizeY = mapGeometry(1,6);
mapLowerLeftX = mapGeometry(1,7);
mapLowerLeftY = mapGeometry(1,8);

%gCords = [ ((x - mapLowerLeftX) / mapCellSizeX) + 0.5 ((y - mapLowerLeftY) / mapCellSizeY) + 0.5 ]
%gCords = [ ceil((x - mapLowerLeftX) / mapCellSizeX), ceil((y - mapLowerLeftY) / mapCellSizeY) ];
cellX = ceil((x - mapLowerLeftX) / mapCellSizeX + 0.5);
cellY = ceil((y - mapLowerLeftY) / mapCellSizeY + 0.5);

mapCellsX = mapGeometry(1,1);
mapCellsY = mapGeometry(1,2);

if bOutsideIsInf == true
    if (cellX < 1) || (cellX > mapCellsX)
        cellX = inf;
    end
    if (cellY < 1) || (cellY > mapCellsY)
        cellY = inf;
    end
else % bOutsideIsInf == false
    if (cellX < 1)
        cellX = 1;
    end
    if (cellY < 1)
        cellY = 1;
    end
    if (cellX > mapCellsX)
        cellX = mapCellsX;
    end
    if (cellY > mapCellsY)
        cellY = mapCellsY;
    end
end


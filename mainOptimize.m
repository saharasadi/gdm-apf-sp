% Description: function mainOptimize
%
% Called by "callOptimize.m"
%
% This code, reads experiment setup and the input data for the given
% experimentLabel and does a grid search on the given interval and steps of
% meta-parameters. It returns a table where the corresponding metric values
% of each set of parameters for tv and test sets are stored. To find the
% set that minimizes the metric, run callAnalyzeBasic.m or
% callAnalyzeMetric.m.
% 
% input: 
%   experimentLabel: name of the input data file
%   experimentType: the type of the experiment and the folder containing the input data file.
%   bNormalizeTime, bEpsilone: are old constants that haven't been
%       refactored. There are set to false.
%   resultFolder: Path to store the output.
%   scale: For basic GDM this is set to 1 otherwhite indicates the fraction
%       for subsampling.
%   itr: whether or not we need to iterate.
%
%% Implementaiton: mainOptimize ==========================

function [optmizedResult, resultFilePath] = mainOptimize(experimentLabel, experimentType, ...
    inputFolder, resultFolder, strStartTimeId, ...
    bEpsilon, bNormalizeTime, bNormalizeData, bVisualizeData, bSaveLogs, ...
    gdmType, metric, ...
    startTime, endTime, ...
    bConstantCellSize, constantCellSize, ...
    scale, itr, totalSamples)

%% Initialization =========================================================

%output path
resultFilePath = [resultFolder '/' experimentType '/' experimentLabel '/' gdmType '/' strStartTimeId];

if ~exist(resultFilePath, 'dir')
    mkdir(resultFilePath);
end

% set kernel constants
weightVarianceSigmaFactor = 1.0;
maxKernelEvalRadiusFactor = 4.0;

% set flags for conditional operations such as plotting, saving logs, etc.
bVerbose = false;
bSaveVisualization = false;

% set epsilon (not used anymore) but it is not removed since requires code
% refactoring.
epsilon = 0.0000001;

%% Select experiment setup to experimentLabel =====================
[experimentType, fileName, startInd, endInd, sweepsInd, ...
    mapCenter, mapSize, ...
    sourceLocation, gasType, experimentSubDescr] = selectExperiment(experimentLabel);

%% Extract input data =====================================================

% extract input data
simData = extractInputData(inputFolder, experimentType, ...
    experimentLabel, fileName, startTime, endTime, startInd, endInd, ...
    bNormalizeData, bNormalizeTime, bVisualizeData);

RandStream.setGlobalStream ...
     (RandStream('mt19937ar','seed',sum(100*clock)));

pos = [];
rd = randi(size(simData,1), [1 totalSamples]); %received as input

pos =  [pos; simData(rd,3) simData(rd,4)];
figure; 
plot(pos(:,1), pos(:,2), '--rx');
inputData = simData(rd, :);

targetTime = max(inputData(:,1));
inputData(:,2) = (inputData(:,2) - min(inputData(:,2)))...
    ./(max(inputData(:,2))-min(inputData(:,2)));


%% Select partitioning cuts ===============================================
strTestSetSelectionId = 'contPartTestSetSelection'; % partitions the data into parsTestSetSelection(2) contiguous sets and selects the parsTestSetSelection(1)th of these sets as test set
parsTestSetSelection = [5 5 0 0]; % %(1)th of %(2) parts with indices from %(3) to %(4)
strTvSetSelectionId = 'contTvSetSelectionFixed'; % partitions the data into parsTvSetSelection(2) contiguous sets and selects the parsTestSetSelection(1)th of these sets as validation set, the rest is used for training
parsTvSetSelection = [4 4]; % %(1)th of %(2) parts is the validation set
numOfSamples = size(inputData, 1);

tvIndexSet = 1:size(inputData,1);
trainIndexSet = 1:1:round((parsTestSetSelection(1,1)-1)*size(inputData)/parsTestSetSelection(1,1));
valIndexSet = max(trainIndexSet)+1:1:size(inputData,1);

%% Set the search interval and steps for each meta-parameter ==============
[minKernelWidth, maxKernelWidth, minCellSize, maxCellSize, ...
    minTimeScale, maxTimeScale, ...
    stepKernelWidth, stepCellSize, stepTimeScale] = selectMetaParametersIntervals(experimentLabel);

%% Parameter space search to create GDM for every givent targetTime. ======

optimizationSet = [];
optNLPD = 10000;
optKernelWidth = 0; 
optCellSize = 0; 
optTimeScale = 0;

constantCellSize = 0.25;
if bConstantCellSize
    minCellSize = constantCellSize;
    maxCellSize = constantCellSize;
end

if strcmp(gdmType, '2D')
    minTimeScale = 0.0;
    maxTimeScale = 0.0;
end


for kernelWidth = minKernelWidth:stepKernelWidth:maxKernelWidth    
    for cellSize = minCellSize:stepCellSize:maxCellSize 
        if kernelWidth <= cellSize 
            continue;
        end
        for timeScale = minTimeScale:stepTimeScale:maxTimeScale

            % Initialize the geometry
            mapCellSize = [cellSize, cellSize];
            [numOfMapCells, mapCenter, mapCellSize, mapLowerLeft] = getMapGeometry(mapCenter, mapSize, mapCellSize);
            mapGeometry = [numOfMapCells, mapCenter, mapCellSize, mapLowerLeft];

            % Initialize kernel variables
            maxKernelEvalRadius = maxKernelEvalRadiusFactor * kernelWidth;
            weightVarianceSigma = weightVarianceSigmaFactor * 1 / ( sqrt(2.0 * pi) * kernelWidth );
            weightVarianceSteepness = weightVarianceSigma * weightVarianceSigma;

            % Create GDM for training set
            [trainGdmMap, kGdmWeightMin, kGdmWeightMax, kGdmMapMin, kGdmMapMax, totalMean, ...
                kGdmVarMin, kGdmVarMax, sigma2Tot] = ...
                createGDM(inputData, trainIndexSet, ...
                    mapGeometry, ...
                    kernelWidth, maxKernelEvalRadius, weightVarianceSteepness, ...
                    timeScale, targetTime, ...
                    bVerbose);

            % Evaluate GDM on the validation set  
            [ trainAvgErr, numOfPredictionsMade, ...
                trainNLPD, numOfPointsForLikelihoodEval] = evaluateGDM(trainGdmMap, ...
                mapGeometry, inputData, valIndexSet, bEpsilon, epsilon);

            if trainNLPD < optNLPD
                optNLPD = trainNLPD;
                optKernelWidth = kernelWidth;
                optCellSize = cellSize;
                optTimeScale = timeScale;
            end

            % Print out the results
    %         fprintf('(c = %.2f, sigma = %.2f)==> (NLPD_train = %.4f, err = %.4f)\n', ...
    %             cellSize, kernelWidth, trainNLPD, trainAvgErr);

            % Save the result in the optimizationSet table
            optimizationSet = [optimizationSet; kernelWidth, cellSize, timeScale, ...
                trainNLPD, trainAvgErr];
        end
    end
end

% set the ending time for logging purpose
endTime = clock;
strEndTimeId = [sprintf('%i',endTime(1)) '_' ...
    sprintf('%02i',endTime(2)) '_' sprintf('%02i',endTime(3)) '-' ...
    sprintf('%02i',endTime(4)) '_' sprintf('%02i',endTime(5)) '_' sprintf('%02.0f',endTime(6))];

% Saves the log in a txt file and save the optimizationSet.
if bSaveLogs
    saveLogsOptimization( optimizationSet, ...
        resultFilePath, inputFolder, resultFolder, ...
        strStartTimeId, strEndTimeId, metric, ...
        trainIndexSet, valIndexSet, [], ...
        strTestSetSelectionId, parsTestSetSelection, strTvSetSelectionId, parsTvSetSelection, ...
        gdmType, experimentType, experimentLabel, fileName, gasType, sourceLocation, ...
        mapCenter, mapSize, ...
        optKernelWidth, optCellSize, optTimeScale, ...
        minKernelWidth, minCellSize, minTimeScale, ...
        maxKernelWidth, maxCellSize, maxTimeScale, ...
        stepKernelWidth, stepCellSize, stepTimeScale, ...
        targetTime, ...
        bNormalizeData, bSaveVisualization, ...
        itr, scale);
end

[minVal, minIdx] = min(optimizationSet(:,4));
optKernelWidth = optimizationSet(minIdx, 1);
optCellSize = optimizationSet(minIdx, 2);
optTimeScale = optimizationSet(minIdx, 3);
optTrainNLPD = optimizationSet(minIdx, 4);
optTrainAvgErr = optimizationSet(minIdx, 5);

optmizedResult = optimizationSet(minIdx, :);

% Print out the results
%fprintf('optimized----------------------------\n');
fprintf('opt %i: (c = %.2f, sigma = %.2f)==> (NLPD_train = %.4f, err = %.4f)\n', ...
    itr, optCellSize, optKernelWidth, optTrainNLPD, optTrainAvgErr);

end
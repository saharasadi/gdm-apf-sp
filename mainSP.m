%% This program calls sensor planning for a given configuration
%

%% Initial Setup
clear
clc
close all

totalNumSamples = 300; 
totalNumRun = 10;
bParallel = true;

% Select Sampling config
[samplingMethod, selectionType] = getSamplingConfig();

% Currently only 'basic' Kernel DM+V is available.
% Select from : ['basic', 'wind', 'time', 'learn'];
gdmMethod = 'basic';   

% Select form: 'coverage', 'plume', 'distance', 'KL', 'source'
measure = 'KL';

% flag to build a map with all data as ground truth.
bCreateGroundTruth =  true;

% flag to calculate GDM for the given cellsize and kernel width.
bCreateGDM = true;

% flag to run evaluation
bEvaluatePerformance = false;

% flag to display the grpahs generated in the evaluation step
bShowVisualize = false;

% Select Experiment Setup
[experimentType, experimentLabel, experimentSubDescr, logFileType, ...
    mapCenter, mapSize, ...
    srcLocation, ...
    initInd, endInd, sweepsInd, ...
    calibrationFile, ...
    time, margin] = selectExperimentSimulation();

samplingPoints = zeros(totalNumSamples, 2);

% Select Sampling Positions
if (strcmp(samplingMethod, 'APF'))
    combination = selectSensorPlanning();
    %combination = [0.0 0.0 0.0 1.0];
    %    0.0 0.1 0.0 0.9;
    %    0.1 0.1 0.0 0.8;
    %    0.1 0.8 0.0 0.1;
    %    0.0 0.5 0.0 0.5];
else
    combination = [0.0 0.0 0.0 0.0]; %selectSensorPlanning();
    samplingPoints = getSamplingLocations(totalNumSamples, samplingMethod, mapSize, margin);
end

if (bCreateGroundTruth)
    [P, PVar] = doCreateGroundTruth();
end

coeff = combination(combination(:,3) == 0, :);
startIdx = 1;
endIdx = size(coeff, 1);

if (bCreateGDM)
    if (bParallel)
        %PARPOOL close force
        delete(gcp('nocreate'))
        parpool('local',2)
    end

    if (bParallel)
        parfor count=startIdx:endIdx
            for runNum=1:1:totalNumRun
                samplingPoints = getSamplingLocations(totalNumSamples, samplingMethod, mapSize, margin);
                doCreateGDM(samplingMethod, selectionType, coeff(count,:), ...
                    samplingPoints,totalNumSamples, runNum, P, PVar); 
            end
        end
    else
        for count=startIdx:endIdx
            for runNum=1:1:totalNumRun
                samplingPoints = getSamplingLocations(totalNumSamples, samplingMethod, mapSize, margin);
                doCreateGDM(samplingMethod, selectionType, coeff(count,:), ...
                    samplingPoints,totalNumSamples, runNum, P, PVar);
            end
        end
    end
    
     if (bParallel)
        delete(gcp('nocreate'))
     end

end

if (bEvaluatePerformance)
    for i=startIdx:endIdx
        doEvaluatePerformance(samplingMethod, selectionType, coeff(i,:), ...
            totalNumRun, totalNumSamples, bShowVisualize, measure);
    end
end

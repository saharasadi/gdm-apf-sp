function [X selectedPos potential score] = doAPFSensorPlacement(score, simData, predModel, cellSize, mapSize, n_sp, ...
    maxRepuls, combination, pos, selectionType)

coeff = combination;
selectedPos = [];
X = [];

% INITIALIZE APF VARIABLES ============================
numX = mapSize(1,1)/cellSize;
numY = mapSize(1,2)/cellSize;
%repuls = zeros(numX, numY, 1);
sumMean = zeros(numX, numY, 1);
sumVar = zeros(numX, numY, 1);
sumConf = zeros(numX, numY, 1);
distance = zeros(numX, numY, 1);
APF = zeros(numX, numY, 1);
%score = ones(numX, numY, 1);

tv = predModel(:,:,5);   
tw = predModel(:,:,3);  
tr = predModel(:,:,4);   
tx = predModel(:,:,1);  
pX = tx(:); 
ty = predModel(:,:,2);  
pY = ty(:); 
tc = 1-predModel(:,:,6); 
pC = tc(:);
repuls = zeros(size(tx,1), size(tx,2), 1);
initAPF = zeros(size(tx,1), size(tx,2), 1);
noRepulseAPF = zeros(size(tx,1), size(tx,2), 1);
%sumConf = tc;
% --------------------------------------------------------   
%% COMMENTED: NO INTEGRATION!

%{
    for m=1:1:size(tx,1)
       for n=1:1:size(tx,2)
            len = sqrt((tx-tx(m,n)).^2+(ty-ty(m,n)).^2);
            sumMean = sumMean + tr(m,n)./exp(len);
            sumVar  = sumVar  + tv(m,n)./exp(len);
            sumConf  = sumConf  + tc(m,n)./exp(len);
       end
    end
%}

sumMean = tr;
sumVar = tv;
sumConf = tc;

gaussianFactor = 1;% / ( sqrt(2.0 * pi) * kernelWidth); % faster than calling an external function  (approx. twice as fast!)
gaussianExpFactor = 1;%- 1 / (2.0 * kernelWidth * kernelWidth);

%    maxRepuls = max(maxRepuls,0);

if (coeff(1,3) > 0)
    for i = 1:1:size(pos,1)
        a = 1; %simData(X(i),6);
        distance = sqrt((pos(i,1)-tx).^2+(pos(i,2)-ty).^2);
        repuls = repuls + a./(gaussianFactor *exp(gaussianExpFactor *distance));
    end
end

potential = [];
% ---------------------------------------------------------
noRepulseAPF = (coeff(1,1) *sumMean(:,:,1)) + (coeff(1,2)*sumVar(:,:,1)) ...
            -(coeff(1,4)*sumConf(:,:,1));
initAPF = noRepulseAPF -(coeff(1,3)*repuls(:,:,1));

spSetPos = [];
spSetX = [];
spSetPotential = [];
spSetDist = [];
area = [];
if (strcmp(selectionType, 'simultaneously'))
%{
    APF = (coeff(1,1) *sumMean(:,:,1)) + (coeff(1,2)*sumVar(:,:,1)) ...
            -(coeff(1,3)*repuls(:,:,1))-(coeff(1,4)*sumConf(:,:,1));
%}
    APF = initAPF;        
    [AA, ix] = sort(APF(:), 'descend');

    preIdxSet = ix; %ix(~ismember(pX(ix), pos(:,1)) & ~ismember(pY(ix), pos(:,2)));
    maxIdxSet = preIdxSet(1:1:min(size(preIdxSet),n_sp)); %5));        
    for i=1:1:size(maxIdxSet,1) % ;-) %??????????
        minIdx = maxIdxSet(i);     
        [~, selectedSensor] = min(sqrt((pX - pX(minIdx)).^2+ (pY - pY(minIdx)).^2));
        spSetPos =  [spSetPos; pX(selectedSensor) pY(selectedSensor)];
        spSetX = [spSetX; selectedSensor];        
        spSetPotential = [spSetPotential; initAPF(selectedSensor)];
        area = find(sqrt((pX - pX(selectedSensor)).^2+ (pY - pY(selectedSensor)).^2) < 0.51);           
        score(area) = score(area) + 1; 
        spSetDist = [spSetDist; ...
            sqrt((pos(size(pos,1),1)-tx(selectedSensor)).^2 + (pos(size(pos,1),2)-ty(selectedSensor)).^2)];
    end
else
    for i=1:1:n_sp
%{            
        APF = (coeff(1,1) *sumMean(:,:,1)) + (coeff(1,2)*sumVar(:,:,1)) ...
                -(coeff(1,3)*repuls(:,:,1))-(coeff(1,4)*sumConf(:,:,1));
%}
        APF = noRepulseAPF - (coeff(1,3)*repuls(:,:,1));
        [AA, ix] = sort(APF(:), 'descend');            
        duplicates = find(APF(:) == AA);

        minIdx = -1;
        if size(duplicates, 1) > 1
           minIdx = duplicates(randi(size(duplicates, 1)), 1); 
        end
%             minDist = 100000;
%             for d=1:size(duplicates, 1)
%                 dist = min(sqrt((pX(duplicates(d,1)) - pos(:,1)).^2 ...
%                     + (pY(duplicates(d,1)) - pos(:,2)).^2));
%                 if dist < minDist
%                     minDist = dist;
%                     minIdx = d;
%                 end
%             end

        if minIdx < 0
            preIdxSet = ix; 
            maxIdxSet = preIdxSet(1); 
            minIdx = maxIdxSet(1);
        end
%             [~, selectedSensor] = min(sqrt((pX - pX(minIdx)).^2+ (pY - pY(minIdx)).^2));
        selectedSensor = minIdx;
        spSetPos =  [spSetPos; pX(selectedSensor) pY(selectedSensor)];
        spSetX = [spSetX; selectedSensor];  
        spSetPotential = [spSetPotential; initAPF(minIdx)];
        area = find(sqrt((pX - pX(selectedSensor)).^2+ (pY - pY(selectedSensor)).^2) < 0.51);          
        score(area) = score(area) + 1;           
        a = 1; %inData(X(i),7);!!!!!  
        distance = sqrt((tx(minIdx)-tx).^2 + (ty(minIdx)-ty).^2);
        repuls = repuls + a./(gaussianFactor * exp(gaussianExpFactor *distance));
        spSetDist = [spSetDist; ...
             sqrt((pos(size(pos,1),1)-tx(minIdx)).^2 + (pos(size(pos,1),2)-ty(minIdx)).^2)];
    end
    APF = initAPF;
end

[~, id] = max(score(spSetX(:,1))./spSetDist(:,1));
score(sqrt((pX - spSetPos(id,1)).^2+ (pY - spSetPos(id,2)).^2) < 0.51) = 0;
selectedPos =  [selectedPos; spSetPos(id,1) spSetPos(id,2)];
X = [X; spSetX(id)];        
potential = [potential; initAPF(spSetX(id))];

end

function [ P, PVar, kGdmvMap ] = doBuildGDM(inputData, time, ...
    mapCenter, mapSize, experimentLabel)

    % parameter initialization/selection (TODO) ==========================
    [kernelWidth, cellSize, timeScale] = selectMetaParameters(experimentLabel);
    % (0.1) Parameters of the algorithm
    mapCellSize = [ cellSize cellSize ]; 
    weightVarianceSigmaFactor = 1.0;
    maxKernelEvalRadiusFactor = 4.0;
        
    % (1.4) Dependent parameters of the Kernel GDM+V algorithm
    maxKernelEvalRadius = maxKernelEvalRadiusFactor * kernelWidth;
    weightVarianceSigma = weightVarianceSigmaFactor * normpdf(0,0,kernelWidth);
    weightVarianceSteepness = weightVarianceSigma * weightVarianceSigma;

    bVerbose = false;

    % (1.5) Map geometry
    [ numOfMapCells, mapCenter, mapCellSize, mapLowerLeft ] = ...
        getMapGeometry(mapCenter, mapSize, mapCellSize);

    mapGeometry = [ numOfMapCells mapCenter mapCellSize mapLowerLeft ];


    % groundTruth = buildMap(simData); ===================================

    trainingSetIndices = 1:1:size(inputData(:,1));

    [kGdmvMap, ...
        kGdmWeightMin, kGdmWeightMax, ...
        kGdmMapMin, kGdmMapMax, totalMean, ...
        kGdmVarMin, kGdmVarMax, sigma2Tot] ...
        = ...
        kernelGDMpV(inputData, trainingSetIndices, ...
        mapGeometry, ...
        kernelWidth, maxKernelEvalRadius, weightVarianceSteepness, ...
        bVerbose);

    map = kGdmvMap(:,:,4); %Distribution Mean Reference
    map_Var= kGdmvMap(:,:,5); %Distribution Variance Reference
    P = (map(:))';
    PVar = (map_Var(:))';

end
# README #

* Developed by: Sahar Asadi
* Last date modified: 2017-02-01

This project is developed by Sahar Asadi and partially in collaboration with 
"Patrick Neumann from BAM research center. To read more:
Autonomous Gas-Sensitive Microdrone: Wind Vector Estimation and Gas Distribution Mapping. 
Robotics Automation Magazine, IEEE, 19:1, 2012, pp. 50-61."

## Content ##
This project extends the two-dimensional vanilla Kernel DM+V to a time-dependent one. 
It includes measuring performance of different sensor planning methods. 
The GDM model is Kernel DM+V. 
Different sensor planning methods are: APF, random, horizontap_sweep, vertical_sweep
Different evaluation measures are:
* KL distance
* Distance from source
* Coverage of the area
* Coverage of the plume
* Distance to the center line of the plume

You can create ternary plots of the results. For this purpose and external package
is used (https://github.com/alchemyst/ternplot).

## HOW TO ##
For FAQ on how to use this project, read HOWTO.txt.

## Contribution ##
This work will be presented in the dissertation "Towards Dense Air Quality Monitoring:
Time-Dependent Statistical Gas Distribution Modelling
and Sensor Planning", Orebro University, in print, 2017.

## Contact ##
For further information, please check out [http://aass.oru.se/Research/Learning/index.html](Link URL)
or contact [sahar.asadi@oru.se](Link URL).
function [] = doEvaluateDistanceToSource(experimentType, mapType, samplingMethod, dataType, inSpConfig, ...
            gdmConfig, source, totalNumRun, totalRunSamples, ...
            bShowVisualize)


    bAPF = true;
    resultPath = ['result/' experimentType '/' samplingMethod];
    mkdir(resultPath);

    x = [1:1:totalRunSamples]';

    mSrc_apf = [];
    vSrc_rnd = [];
    modelType = 'fulldata';    
    spConfig_apf = inSpConfig; 
    
   dirNamePrefix = ['output/' experimentType '/' 'APF']
   for itr=1:totalNumRun
       pos = zeros(totalRunSamples+1, 2);
       dirName = [dirNamePrefix '/' 'SP-' num2str(itr) '-' spConfig_apf];

       fileName = [dirName '/' 'KL' '-' experimentType '-' 'filaments100' '_' modelType '-' ...
           gdmConfig '-' spConfig_apf '-' dataType '.mat'];
       if (exist(fileName, 'file') == false)
               return
       end
       data = load(fileName);       
       pos(:,1:2) = data.pos(:,1:2); 
       dist = zeros(totalRunSamples+1, 1);
       dist(:,1) = sqrt((pos(:,1) - source(1,1)).^2 + (pos(:,2) - source(1,2)).^2);

       mSrc_apf = [mSrc_apf dist(:,1)];
   end        
   muSrc_apf = mean(mSrc_apf, 2);
   sdSrc_apf = std(mSrc_apf, 0, 2);

    delay = 0;
    maxY = 18;  minY = 0;    %!!!
    maxX = totalRunSamples; minX = 1;    %!!!

    if (strcmp(mapType, 'variance'))
        col = 2;
        maxY = 7;%'variance'
    else
        col = 1; %'mean'
    end

    close all;
    h = figure('Units', 'pixels', 'Position', [100 100 1400 800]);
    if bShowVisualize
        set(h, 'visible', 'on');
    else
        set(h, 'visible', 'off');
    end
    
    txtTitle = ({['Distance of sampling points to gas source location'] ...
        ['using APFSP sampling strategy (' experimentType 'experiment)']}); 
    hTitle = title(txtTitle);

    hXLabel = xlabel('sampling iteration');          
    hYLabel = ylabel('Distance from source location');

    axis([minX+delay maxX minY maxY]);
    set(gca, 'LineWidth', 1);
    box on

    set( hTitle, 'FontSize', 9, 'FontWeight' , 'bold');
    set( hXLabel, 'FontSize', 7, 'FontWeight' , 'bold');
    set( hYLabel, 'FontSize', 7, 'FontWeight' , 'bold');

    lineSrc_apf = [];
    lineSrc_apf = line(x, muSrc_apf(1:totalRunSamples, 1));
    set(lineSrc_apf, 'Color',  [1 0 0], 'LineWidth', 1);
    
    hold on;
    
    file = [resultPath '/' 'result-' num2str(delay) '-' experimentType '_' mapType '-' gdmConfig '-' spConfig_apf '-' 'dist-to-srouce'];
    %hold off
    saveas(h, [file '.fig'], 'fig');
    print(h, [file '.png'], '-dpng', '-r300');
    save([file '.mat'], 'muSrc_apf', 'sdSrc_apf');

end
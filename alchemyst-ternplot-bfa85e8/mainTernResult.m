%% Simple two-d plot
clear all
close all
clc

coeff = selectSensorPlanning();
combination = coeff(coeff(:,3) == 0, :);
A = combination(:,1)';
B = combination(:,2)';
C = 1 - (A+B);

fileID = fopen('distance.txt','r');
formatSpec = '%f\t%f\t%f';
sizeA = [3 Inf];
result = fscanf(fileID,formatSpec,sizeA);
fclose(fileID);
type = 'Travelling Distance';

for i=1:1:3
    data = result(i,:);
    h = figure('Units', 'pixels', 'Position', [100 100 1100 800]);
    experiment = 'Sim-Obstacle-Mid';
    if (i == 1)
        experiment = 'Sim-No-Obstacle';
    elseif (i == 2)
            experiment = 'Sim-Obstacle-Up';
    end
    txtTitle = ({['Impact of Meta-parameter Selection on ' type ...
        '(' experiment ' Experiment)']}); 
    hTitle = title(txtTitle);
    set( hTitle, 'FontSize', 14, 'FontWeight' , 'bold');
    axis off
    hold on
    ternplot(A, B, C, '.'); 
    %terncontour(A, B, C)

    hold on
    ternlabel('Mean', 'Variance', 'Confidence');
    ternpcolor(A, B, data); 
    hold on
    colorbar('FontSize',14, 'FontWeight','bold');
    hold on
    shading interp
    axis off
    file = [type '-' experiment];
    saveas(h, [file '.fig'], 'fig');
    
end

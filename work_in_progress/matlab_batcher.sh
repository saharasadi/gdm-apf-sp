#!/bin/sh

export LD_LIBRARY_PATH=/home/sahar/MATLAB/sys/os/glnx86:/home/sahar/MATLAB/runtime/glnx86/:/home/sahar/MATLAB/bin/glnx86:/home/sahar/MATLAB/sys/java/jre/glnx86/jre/lib/i386/native_threads:/home/sahar/MATLAB/sys/java/jre/glnx86/jre/lib/i386/server:/home/sahar/MATLAB/sys/java/jre/glnx86/jre/lib/i386:

export XAPPLRESDIR=/home/sahar/MATLAB/X11/app-defaults

matlab_exec=matlab

killall -9 MATLAB

#X = "run doAllCombinations"
#echo ${X} > matlab_command_${2}.m
#cat matlab_command_${2}.m
#${matlab_exec} -nodisplay -nosplash -r < matlab_command_${2}.m
#rm matlab_command_${2}.m

${matlab_exec} -nodisplay -nosplash -r "run doAllCombinations" > out
rm out

%% This program calls all combinations of available importance weights of
%% objectives that we want to test and calls the sensor planning to be run
%% for each of these combinations.

clear
clc
close all

combination = selectSensorPlanning();
       
totalEpoch = 20;
startIdx = 1;
totalRun = totalEpoch * floor(size(combination, 1)/2);

matlabpool close force
matlabpool open local 4

endIdx = totalRun * 2; %size(combination, 1);
samplingType = 'APF'; %['random'; 'APF'; 'vertical_sweep'; 'horizontal_sweep'];

parfor i=startIdx:endIdx
    combinationIdx = floor(i/totalEpoch);
    epoch = rem(i, totalEpoch);
    coeff = combination(combinationIdx, :);
    doAPFSensorPlanning(epoch, coeff, samplingType); 
end


%%
%=========================================================================
% This is a program to evaluate sensor planning output using several
% evaluation measures and also plotting/map generation functions
% Sahar Asadi, 2012-02-01
%=========================================================================
%%

clear;
clc;
close all;

%% To select one of the defined evaluation measures =======================
%'EMDist' --> Earth Movers Distance between two distributions
%'histogramDist' --> histogram based distance between two distributions
%'quadDist' --> Quadratic Distance between two distributions (this is
%               histrogrambased too
%'KL' --> Kullback-Leibler Distance between two ditributions
%'scaledMap' --> NOTE: this is not working but it meant to scales all generated maps
%'diffMap' --> Generates the difference map of two created distribution models
%'distance' --> to sum the total Euclidean distance between each two consecutive sampling points
%               if the experiment is performed several times, it averages
%               over all.
%'coverage' --> to compute the total divergance from the horizontal and vertical center lines
%               of the environment (distances are Euclidean). If the
%               experiment is performed serveral times, it averages over
%               all.
%'sourceEstimate' --> to estimate the source location from the created
%                     predictive mean and variance gas distribution maps (estimation based on
%                     different criteria are computed).
%'plume' --> NOTE: this is not implemented but it meant to compute ratio of the samples that
%            are sampled inside the plume.

measure = 'KL'; 
experiment = 'obstacle';

switch measure
%% KL creates plots of KL results already calculated in running experiments.
    case  'KL'
        bRand = true; %to select to use random sampling KL values in the splots or not.
        bAPF = true; %to select to use APF-based sampling KL values in the splots or not.
        runNum = 1; %number of times each experiment is repeated with the same meta-parameters
        iterationNum = 70; %number of collected samples at each experiment
        mapType = 'variance'; %select between mean and variance
        modelType = 'APF';    %select between 'APF; and 'random' (NOTE: later sweeping_horizontal and sweeping_vertical will be added).
        dataType = 'KLValues'; %'FULLDATA'; 'Models'; 'Positions'; 'SAMPLES'; 'FullModel'
        doEvaluateKL(experiment, mapType, modelType, dataType, bRand, bAPF, ...
            runNum, iterationNum, bShowVisualize);
        
    case 'scaledMap'
        mapType = 'mean';
        dataType = 'Models';
        modelType  = 'APF';
        runIdx = 20;
        iterationIdx = 300;
        doGenerateMaps(experiment, mapType, modelType, dataType, runIdx, iterationIdx);
 
%% diffMap creates the difference map between two created models
    case 'diffMap'
        mapType = 'mean';
        dataType = 'Models';
        modelType_1  = 'APF';
        modelType_2 = modelType_1;
        runIdx_1 = 1; %which run of the experiment #1
        runIdx_2 = runIdx_1; %which run of the experiment #2        
        iterationIdx_1 = 70; %which sampling iteratoin of experiment #1
        iterationIdx_2 = 70; %which sampling iteratoin of experiment #2        
        coeff_1 = [0.1 0.4 0.1 0.4]; %which combination as experiment #1
        coeff_2 = coeff_1; %which combination as experiment #2
        doDifferMaps(experiment, mapType, modelType_1, modelType_2, dataType, ...
            runIdx_1, runIdx_2, iterationIdx_1, iterationIdx_2, coeff_1, coeff_2);       
        
%% distance: averages the total distance travelled by the mobile sensor on all runs of an experiment    
    case 'distance'
        runNum = 1; %number of runs
        iterationNum = 70; % number of samples collected at each run
        modelType = 'APF'; %'random'
        dataType = 'Positions'; %'FULLDATA'; 'Models'; 'Positions'; 'SAMPLES'; 'FullModel'
        doCompDistance(experiment, modelType, dataType, runNum, iterationNum);
%% coverage: averages on the vertical and horizontal distances travelled by the mobile sensor        
    case 'coverage'
        runNum = 20;
        iterationNum = 300;
        modelType = 'APF'; %'random'
        dataType = 'Positions'; %'FULLDATA'; 'Models'; 'Positions'; 'SAMPLES'; 'FullModel'

        doCompCoverage(experiment, modelType, dataType, runNum, iterationNum);
        
    case 'sourceEstimate'
        coeff = selectSensorPlanning();
        runNum = 20;
        iterationNum = 300;
        modelType = 'APF'; %'random'
        dataType = 'Models'; %'FULLDATA'; 'Models'; 'Positions'; 'SAMPLES'; 'FullModel'
        for i=1:1:size(coeff, 1)
            doSourceEstimate( experiment, modelType, dataType, runNum, iterationNum, coeff(i, :));
        end
    case 'plume'
        coeff = [0.1 0.4 0.1 0.4];
        runNum = 20;
        iterationNum = 300;
        modelType = 'APF'; %'random'
        dataType = 'Models'; %'FULLDATA'; 'Models'; 'Positions'; 'SAMPLES'; 'FullModel'
        %doSourceEstimate( experiment, mapType, modelType, dataType, runNum, iterationNum, coeff);
        %doCompPlumeCoverage();
        
    case 'histogramDist'
        runNum = 20;
        iterationNum = 300;
        coeff = selectSensorPlanning(); %[0.1 0.4 0.1 0.4];
        mapType = 'mean';
        modelType = 'APF'; %'random'
        dataType = 'Models'; %'FULLDATA'; 'Models'; 'Positions'; 'SAMPLES'; 'FullModel'
        for i=1:1:size(coeff, 1)
            doHistogramIntersect( experiment, mapType, modelType, dataType, runNum, iterationNum, coeff(i, :) );
        end
            
    case 'quadDist'
        runNum = 20;
        iterationNum = 300;
        coeff = [0.1 0.4 0.1 0.4];
        mapType = 'mean';
        modelType = 'APF'; %'random'
        dataType = 'Models'; %'FULLDATA'; 'Models'; 'Positions'; 'SAMPLES'; 'FullModel'
        doQuadDist( experiment, mapType, modelType, dataType, runNum, iterationNum, coeff );
    
    case 'EMDist'
        runNum = 20;
        iterationNum = 300;
        coeff = selectSensorPlanning(); %[0.1 0.4 0.1 0.4];
        mapType = 'mean';
        modelType = 'APF'; %'random'
        dataType = 'Models'; %'FULLDATA'; 'Models'; 'Positions'; 'SAMPLES'; 'FullModel'
        for i=1:1:size(coeff, 1)
            doEMDist( experiment, mapType, modelType, dataType, runNum, iterationNum, coeff );
        end
    otherwise
        disp('the selected measure is out of the scope');
end

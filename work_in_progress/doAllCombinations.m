%% This program calls all combinations of available importance weights of
%% objectives that we want to test and calls the sensor planning to be run
%% for each of these combinations.

clear
clc
close all

samplingType = 'random'; %['random'; 'APF'; 'vertical_sweep'; 'horizontal_sweep'];
selectionType = 'iteratively';

if (strcmp(samplingType, 'APF'))
    combination = selectSensorPlanning();
%    combination = [0.1 0.1 0.1 0.7];
%                   0.0 0.0 0.0 1.0; ...
%                    0.0 0.0 1.0 0.0; ...
%                    0.1 0.1 0.8 0.0; ...
%                    0.1 0.1 0.0 0.8; ...
%                    0.5 0.2 0.3 0.0; ...
%                    0.5 0.2 0.0 0.3];
else
    combination = [0.0 0.0 0.0 0.0]; %selectSensorPlanning();
end

totalEpoch = 1;
startIdx = 1;
totalRun = totalEpoch * size(combination, 1);

bParallel = true;

if (bParallel)
    delete(gcp('nocreate'))
    parpool('local', 2)
end

endIdx = size(combination, 1);

combinationIdx = 1;
epoch = 1;
coeff = combination(combinationIdx, :);
%doAPFSensorPlanning(epoch, coeff, samplingType, selectionType); 

if (bParallel)
    parfor count=startIdx:endIdx
         combinationIdx = count;
         coeff = combination(combinationIdx, :);
         if (coeff(1,3) == 0)
             for r=1:1:totalEpoch
                epoch = r;
                doAPFSensorPlanning(epoch, coeff, samplingType, selectionType); 
             end
         end
    end
else
    for count=startIdx:endIdx
         combinationIdx = count;
         coeff = combination(combinationIdx, :);
         %if (coeff(1,3) == 0)
             for r=1:1:totalEpoch
                epoch = r;
                doAPFSensorPlanning(epoch, coeff, samplingType, selectionType); 
             end
         %end
    end    
end

if (bParallel)
   delete(gcp('nocreate'))
end

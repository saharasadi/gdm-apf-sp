%This code selects randoml N samples and runs GDM optimization to select
%cell size and kernel width. 

clc;
close all;
clear all;

numSamples = 300;

%% selectExperiment();=================================================
[experimentType, experimentLabel, experimentSubDescr, logFileType, ...
    mapCenter, mapSize, ...
    srcLocation, ...
    initInd, endInd, sweepsInd, ...
    calibrationFile, ...
    time, margin] = selectExperimentSimulation();

%% selectSensors();====================================================
[sensorsUsed, bNormalizeSensorData, sensorsUsedDescr, ...
    expClassDescr] = selectSensors();

    %% select input data ==================================================
experimentPath = 'data';
inputFile = [experimentPath '/' experimentLabel '.log'];

inputData = extractInputDataFromSimSP(inputFile,sensorsUsed,...
    false,calibrationFile);

inputData = inputData((inputData(:,3)  < (mapSize(1,1) - margin(1,2))), :);

%% SELECT A RANDOM INITIAL POSITION ===================================
X = [];
bVerbose = false;
pos = [];

%for now, the initial sensor id is selected randomly
RandStream.setGlobalStream ...
     (RandStream('mt19937ar','seed',sum(100*clock)));

rd = randi(size(inputData,1), [1 numSamples]); %received as input
selectedIdx = rd;
pos =  [pos; inputData(rd,3) inputData(rd,4)];
simData = inputData(rd, :);

%timeTarget = time(2);
simData(:,2) = (simData(:,2) - min(simData(:,2)))...
    ./(max(simData(:,2))-min(simData(:,2)));

    
% flag to decide if we want to run optimization for constant cell size.
bConstantCellSize = false;

%Select numSamples randomly

%